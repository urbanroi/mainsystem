package com.urban.ROI_System.controller;

import com.urban.ROI_System.service.LinkValidationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/link")
@Slf4j
public class LinkValidationRestController {

    @Autowired private LinkValidationService linkService;

    @GetMapping
    public ResponseEntity<Integer> linkValidation(@RequestParam String link) throws Exception {
        return new ResponseEntity<Integer>(linkService.send(link), HttpStatus.OK);
    }
}