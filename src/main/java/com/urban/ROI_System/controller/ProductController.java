package com.urban.ROI_System.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.urban.ROI_System.model.ProductModel;
import com.urban.ROI_System.model.UserModel;
import com.urban.ROI_System.service.ProductService;
import com.urban.ROI_System.util.BeanUtil;
import com.urban.ROI_System.util.Pagination;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/user/product-list")
@Slf4j
public class ProductController {

    @Autowired private ProductService prodService;
    private final int pageSize = 5;

    @GetMapping
    public String productPage(Model model, HttpSession session, @RequestParam(defaultValue = "1") int page, @RequestParam(defaultValue = "none") String keyword) throws JsonProcessingException {
        UserModel userModel = (UserModel) session.getAttribute("loginInfo");
        Pagination pagination = new Pagination(page, (keyword.equals("none")) ? prodService.countByUserModel(userModel) : prodService.countByUserModelAndProductName(userModel, keyword));

        log.info("pagination : " + pagination.toString());

        model.addAttribute("productList", (keyword.equals("none")) ? prodService.findByPage(page, pageSize, userModel) : prodService.findByProductName(page, pageSize, userModel, keyword));
        model.addAttribute("pagination", pagination);

        List<ProductModel> productModelList = prodService.findByUserNo(userModel);
        model.addAttribute("allProductList", productModelList); // pagination X

        model.addAttribute("allProductListJSON", BeanUtil.getObjectMapper().registerModule(new JavaTimeModule()).writeValueAsString(productModelList)); // pagination X

        return "product/product-list";
    }
}