package com.urban.ROI_System.controller;

import com.urban.ROI_System.exception.RegularException;
import com.urban.ROI_System.exception.validity.RegularChecker;
import com.urban.ROI_System.exception.validity.UserRegularChecker;
import com.urban.ROI_System.model.UserModel;
import com.urban.ROI_System.service.ProjectLogService;
import com.urban.ROI_System.service.UserService;
import com.urban.ROI_System.util.ReturnWithAlert;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.Enumeration;

@Controller("/")
@Slf4j
public class MainController {

    @Autowired
    private UserService userService;
    @Autowired
    private ProjectLogService projectLogService;

    @Value("${oAuth.redirectURI}")
    private String oAuthRedirectURI;

    @GetMapping(value = {"/", "index", "main"})
    public String main() {
        return "index";
    }

    @GetMapping("/login")
    public String login() throws Exception {
        return "user/login";
    }

    @PostMapping("/login/action")
    public ModelAndView loginAction(UserModel userModel, HttpSession session) throws Exception {
        if (userService.findByIdAndPw(userModel)) {
            session.setAttribute("loginInfo", userService.findById(userModel.getId()));
            Object beforeUrl = session.getAttribute("beforeUrl");
            if(beforeUrl != null) {
                session.removeAttribute("beforeUrl");
                return new ModelAndView("redirect:"+beforeUrl.toString());
            }
            return new ReturnWithAlert("로그인 되었습니다", "/index");
        } else {
            return new ReturnWithAlert("아이디나 비밀번호가 없거나 틀렸습니다.", "/login");
        }
    }

    @GetMapping("/join")
    public String join() throws Exception {
        return "user/join";
    }

    @GetMapping("/logout")
    public ModelAndView logout(HttpSession session) {
        Enumeration<String> sessionNames = session.getAttributeNames();
        while (sessionNames.hasMoreElements()) session.removeAttribute(sessionNames.nextElement());

        return new ReturnWithAlert("로그아웃 되었습니다", "/index");
    }

    @PostMapping("/join/action")
    public ModelAndView joinAction(UserModel userModel) throws Exception {

        try {
            RegularChecker checker = new UserRegularChecker();
            checker.checkAll(userModel);
        } catch (RegularException e) {
            log.error(e.getMessage());
            return new ReturnWithAlert(e.getMessage(), "/");
        }
        userService.save(userModel);
        return new ReturnWithAlert("성공적으로 회원가입 되었습니다!", "/login");
    }

}