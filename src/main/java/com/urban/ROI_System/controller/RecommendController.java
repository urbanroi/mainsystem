package com.urban.ROI_System.controller;

import com.urban.ROI_System.model.BigCategoryDTO;
import com.urban.ROI_System.model.BigCategoryModel;
import com.urban.ROI_System.model.RecommendModel;
import com.urban.ROI_System.model.SmallCategoryModel;
import com.urban.ROI_System.repository.BigCategoryRepository;
import com.urban.ROI_System.repository.RecommendRepository;
import com.urban.ROI_System.repository.SmallCategoryRepository;
import com.urban.ROI_System.service.RecommendService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/recommend")
@Slf4j
public class RecommendController {

    @Autowired
    BigCategoryRepository bigRepo;
    @Autowired
    SmallCategoryRepository smallRepo;

    @Autowired
    RecommendService recommendService;

    @Autowired
    RecommendRepository recommendRepository;

    @RequestMapping
    public String getRecommendPage() {
        return "recommend/recommend";
    }

    @ResponseBody
    @GetMapping("/bigCategoryList")
    public ResponseEntity<List<BigCategoryDTO>> getBigCategoryList() {
        List<BigCategoryDTO> categoryList = bigRepo.findAllBy();
        return new ResponseEntity<>(categoryList, categoryList.size() == 0 ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    @ResponseBody
    @GetMapping("/bigCategory/{bNo}")
    public ResponseEntity<BigCategoryModel> getBigCategory(@PathVariable("bNo") int no) {
        BigCategoryModel bigCategory = bigRepo.findByNo(no);
        return new ResponseEntity<>(bigCategory, bigCategory == null ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    @ResponseBody
    @GetMapping("/smallCategory")
    public ResponseEntity<List<SmallCategoryModel>> getSmallCategoryList() {
        List<SmallCategoryModel> categoryList = smallRepo.findAll();
        return new ResponseEntity<>(categoryList, categoryList.size() == 0 ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    @ResponseBody
    @GetMapping("/smallCategoryList/{bNo}")
    public ResponseEntity<List<SmallCategoryModel>> getSmallCategoryByBigCategoryNo(@PathVariable("bNo") int no) {
        List<SmallCategoryModel> categoryList = smallRepo.findByBigCategoryModel(bigRepo.findByNo(no));
        return new ResponseEntity<>(categoryList, categoryList.size() == 0 ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    @ResponseBody
    @GetMapping("/smallCategory/{sNo}")
    public ResponseEntity<SmallCategoryModel> getSmallCategory(@PathVariable("sNo") int no) {
        SmallCategoryModel smallCategory = smallRepo.findByNo(no);
        return new ResponseEntity<>(smallCategory, smallCategory == null ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    @ResponseBody
    @GetMapping("/request")
    public ResponseEntity<List<RecommendModel>> recommend(Integer smallCategoryNo,Integer bigCategoryNo,Character gender,Integer age) {
        List<RecommendModel> recommends = recommendService.getSortedRecommendData(smallCategoryNo,bigCategoryNo,age,gender);
//      List<RecommendModel> recommends =  recommendRepository.findAllBySmallCategoryNoAndBigCategoryNoAndGenderAndAgeOrderByRoiDesc(smallCategoryNo,bigCategoryNo,gender,age);
        return new ResponseEntity<>(recommends, recommends.size() == 0 ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }
}