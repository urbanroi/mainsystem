package com.urban.ROI_System.controller;

import com.urban.ROI_System.model.ProductModel;
import com.urban.ROI_System.model.ProjectLogModel;
import com.urban.ROI_System.model.ProjectModel;
import com.urban.ROI_System.repository.BigCategoryRepository;
import com.urban.ROI_System.repository.GtmTagRepository;
import com.urban.ROI_System.repository.ProjectLogRepository;
import com.urban.ROI_System.service.*;
import com.urban.ROI_System.util.Pagination;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/user/project-list")
@Slf4j
public class ProjectController {

    @Autowired private ProjectService projectService;
    @Autowired private CrawlSiteListService siteService;
    @Autowired private ProductLogService logService;
    @Autowired private ProductService prodService;
    @Autowired private PRMethodService prMethodService;

    @Autowired private ProjectLogRepository projectLogRepo;
    @Autowired private ProjectLogService projectLogService;
    @Autowired private GtmTagRepository gtmTagRepository;
    @Autowired private BigCategoryService bigCategoryService;

    private final int size = 10;

    @GetMapping
    public String getProjects(HttpSession session, @RequestParam Long productNo, @RequestParam(defaultValue = "1") int page, Model model) {

        ProductModel productModel = prodService.findByNo(productNo);

        model.addAttribute("totalProjectList", projectService.findByProductNo(productNo));
        model.addAttribute("projectList", projectService.findByPage(page, size, productNo));
        model.addAttribute("logList", logService.findByProductNo(productNo));
        model.addAttribute("crawlSiteList", siteService.findAll());
        model.addAttribute("productNo", productNo);
        model.addAttribute("category", bigCategoryService.getAll());
        try {
            model.addAttribute("productName", productModel.getProductName());
        } catch (NullPointerException e) {
            return "redirect:/user/product-list";
        }

        model.addAttribute("pagination", new Pagination(page, projectService.count(productNo), size));
        model.addAttribute("projectLogList", projectLogService.getProjectLogList(productNo));
        session.setAttribute("productNo", productNo);

        boolean gtmExists = false;
        try {
            if (gtmTagRepository.findByProductModel(productModel).size() > 0) gtmExists = true;
        } catch (NullPointerException e) { }

        model.addAttribute("gtmExists", gtmExists);

        return "project/project-list";
    }

    @GetMapping("/detail/{projectNo}")
    public String getProjectDetailPage(@PathVariable Long projectNo, Model model, HttpSession session) {
        ProjectModel project = projectService.findByNo(projectNo);
        if (project == null) return "redirect:/error/404";

        List<ProjectLogModel> logs = projectLogRepo.findByProjectModel(project);

        model.addAttribute("project", project);
        model.addAttribute("projectLogList", logs);
        model.addAttribute("startDate", project.getProjectStartDate().toLocalDate());
        model.addAttribute("endDate", project.getProjectEndDate().toLocalDate());
        model.addAttribute("prMethodList", prMethodService.findByCrawlSiteListNo(project.getCrawlSiteListModel().getNo()));
        model.addAttribute("category", bigCategoryService.getAll());

        return "project/project-detail";
    }
}