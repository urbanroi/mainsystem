package com.urban.ROI_System.controller;

import com.urban.ROI_System.exception.RegularException;
import com.urban.ROI_System.exception.validity.ProjectRegularChecker;
import com.urban.ROI_System.repository.ProductRepository;
import com.urban.ROI_System.service.GARequestInfo;
import com.urban.ROI_System.util.ServerValue;
import com.urban.ROI_System.vo.Token;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Configuration
@Slf4j
@RequestMapping("/user/oauth")
public class AuthController {

    @Value("${oAuth.clientID}")
    private String oAuthClientId;


    @Value("${oAuth.clientPW}")
    private String oAuthClientPW;
    @Value("${oAuth.redirectURI}")
    private String oAuthRedirectURI;
    @Value("${oAuth.scope}")
    private String scope;

    @Autowired
    private GARequestInfo gaRequestInfo;
    @Autowired
    private ProductRepository pr;

    /**
     * 구글 로그인
     *
     * @param model
     * @return 로그인 창
     */
    @GetMapping("/google-login")
    public String googleLogin(Model model) {
        model.addAttribute("oAuthRedirect", oAuthRedirectURI);
        model.addAttribute("oAuthId", oAuthClientId);
        model.addAttribute("oAuthScope", scope);
        return "oauth/google_login";
    }

    /**
     * @param pn
     * @param session
     * @return
     */
    @ResponseBody
    @GetMapping("/productName/{productName}")
    public ResponseEntity getProductName(@PathVariable("productName") String pn, HttpSession session) {
        session.setAttribute("productName", pn);

        return new ResponseEntity(null, HttpStatus.OK);
    } //getProductName();

    @GetMapping("/redirect")
    public String redirect(String code, HttpSession session) {
        session.setAttribute("popupLevel", 2);
        session.setAttribute("GACheck", "");

        try {
            Token token = gaRequestInfo.getToken(code);
            session.setAttribute("token", token);

            //          GA_AccountInfo userInfo = gaRequestInfo.getGaUserAccount(token); //사용자 정보 요청

            return "redirect:/user/product-list";
        } catch (Exception e) {
            log.error(e.toString());
            return "redirect:/error/500";
        }
    } // redirect();

    @ResponseBody
    @GetMapping("/check-GA")
    public ResponseEntity checkGA(HttpSession session) {
        if (session.getAttribute("GACheck") != null) {
            session.removeAttribute("GACheck");
            return new ResponseEntity(null, HttpStatus.OK);
        } else {
            return new ResponseEntity("", HttpStatus.valueOf(400));
        }
    } //checkGA();

    @ResponseBody
    @GetMapping("/request-ticket") //Ticket 발급
    public ResponseEntity<String> requestTicket(HttpSession session, @RequestParam String name, HttpServletResponse response) {
        session.setAttribute("popupLevel", 3);

        session.setAttribute("productName", name);

        try {
            new ProjectRegularChecker().nameCheck(name);
            String ticket = gaRequestInfo.getTicket(name, session);
            String serviceAccount = ServerValue.get().getServiceAccount();
            Cookie cookie = new Cookie("serviceAccount", serviceAccount);
            cookie.setMaxAge(60 * 180); // 쿠키 만료 시간 = 세션 만료 시간(180분)
            cookie.setPath("/");
            response.addCookie(cookie);  // 쿠키로 서비스 계정 전송

            return new ResponseEntity<String>("https://analytics.google.com/analytics/web/?provisioningSignup=false#/termsofservice/" + ticket, HttpStatus.valueOf(200));
        } catch (IOException e) {
            log.error(e.toString());
            return new ResponseEntity<>("", HttpStatus.valueOf(406));
        } catch (RegularException e) {
            return new ResponseEntity(e, HttpStatus.valueOf(400));
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        }
    } // requestTicket();

    // GA 계정 생성시 필요한 리디렉션 URL
    @GetMapping("/ga-create-success")
    public String gaCreateSuccess(Model model, String accountId) {
        log.info("accountId : " + accountId);
        model.addAttribute("accountId", accountId);

        return "oauth/ga_alert";
    } // gaCreateSuccess();
} //class
