package com.urban.ROI_System.controller;

import com.urban.ROI_System.util.ReturnWithAlert;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("${server.error.path:${error.path:/error}}")
public class ErrorController {
	
	@RequestMapping("/404")
	public String error404(HttpSession session) {
		session.setAttribute("msg","페이지를 찾을 수 없습니다.");
		session.setAttribute("red","/");
		return "redirect:send-alert";
	}

	@RequestMapping("/400")
	public String error400(HttpSession session) {
		session.setAttribute("msg","유효하지 않은 접근입니다");
		session.setAttribute("red","/");
		return "redirect:send-alert";
	}

	@RequestMapping("/500")
	public String error500(HttpSession session) {
		session.setAttribute("msg","서버에서 오류가 발생하였습니다. 잠시후 시도해 주세요");
		session.setAttribute("red","/");
		return "redirect:send-alert";
	}

	@RequestMapping(value = "/send-alert")
	public ModelAndView alert(HttpServletResponse response, HttpServletRequest request) {
		try {
			String msg = request.getSession().getAttribute("msg").toString();
			String red = request.getSession().getAttribute("red").toString();
			request.getSession().removeAttribute("msg");
			request.getSession().removeAttribute("red");
			return new ReturnWithAlert(msg, red);
		} catch (Exception e) {
			return new ReturnWithAlert("서버에서 오류가 발생하였습니다. 잠시후 시도해 주세요", "/");
		}
	} // alert();
} //class