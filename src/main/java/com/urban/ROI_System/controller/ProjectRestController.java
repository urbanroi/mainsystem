package com.urban.ROI_System.controller;

import com.urban.ROI_System.exception.RegularException;
import com.urban.ROI_System.exception.validity.ProjectRegularChecker;
import com.urban.ROI_System.exception.validity.RegularChecker;
import com.urban.ROI_System.model.PRMethodModel;
import com.urban.ROI_System.model.ProductModel;
import com.urban.ROI_System.model.ProjectModel;
import com.urban.ROI_System.repository.ProductRepository;
import com.urban.ROI_System.repository.ProjectLogRepository;
import com.urban.ROI_System.repository.ProjectRepository;
import com.urban.ROI_System.service.DataUpdateService;
import com.urban.ROI_System.service.PRMethodService;
import com.urban.ROI_System.service.ProjectService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@RestController
@RequestMapping("/rest/project")
@Slf4j
public class ProjectRestController {

    @Autowired
    private ProjectService projectService;
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private PRMethodService methodService;
    @Autowired
    private DataUpdateService dataUpdateService;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ProjectLogRepository projectLogRepository;

    @ApiOperation(value = "프로젝트 수정", notes = "프로젝트를 수정한다.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "정상"),
            @ApiResponse(code = 400, message = "잘못된 접근")
    })
    @PatchMapping("/update/data")
    public ResponseEntity updateData(@RequestParam Long productNo) {
        ProductModel pm = productRepository.findByNo(productNo);
        if (pm == null) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        } else {
//            dataUpdateService.updateData(pm);
            return new ResponseEntity(HttpStatus.OK);
        } //if~else
    }

    @PostMapping
    public ResponseEntity<String> saveProject(@ModelAttribute ProjectModel projectModel, @RequestParam String startDate, @RequestParam String endDate,
                                              @RequestParam(defaultValue = "") String method, @RequestParam(defaultValue = "") String source, @RequestParam(defaultValue = "") String medium
    ) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        projectModel.setProjectStartDate(LocalDateTime.parse(startDate, formatter));
        projectModel.setProjectEndDate(LocalDateTime.parse(endDate, formatter));

        if (projectModel.getCrawlSiteListModel().getNo() == 999) {
            projectModel.getCrawlSiteListModel().setName(source);
            method = medium;
        }

        try {
            RegularChecker rc = new ProjectRegularChecker();
            rc.checkAll(projectModel);
            projectService.save(projectModel, method);

            return new ResponseEntity<>("success", HttpStatus.OK);
        } catch (RegularException e) {
            log.info("RegularException");
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        } // try~catch

    } // saveProject();

    @GetMapping("/pr-method/list")
    public ResponseEntity<List<PRMethodModel>> getPrMethodList(@RequestParam Long siteNo) {
        return new ResponseEntity<>(methodService.findByCrawlSiteListNo(siteNo), HttpStatus.OK);
    } //getPrMethodList();

    @GetMapping("find")
    public ResponseEntity projectNameDuplicateCheck(@RequestParam String projectName, @RequestParam Long productNo) {

        HttpStatus hs = HttpStatus.OK;

        if (projectService.projectNameDuplicateCheck(projectName, productNo))
            hs = HttpStatus.BAD_REQUEST;

        return new ResponseEntity(hs);
    } //projectNameDuplicateCheck();

    @PatchMapping("/{no}")
    public ResponseEntity<String> patchProject(@PathVariable("no") Long no, @ModelAttribute ProjectModel projectModel, @RequestParam String startDate, @RequestParam String endDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        projectModel.setProjectStartDate(LocalDateTime.parse(startDate, formatter));
        projectModel.setProjectEndDate(LocalDateTime.parse(endDate, formatter));

        log.info(projectModel.toString());
        projectService.updateProject(projectModel);

        return new ResponseEntity<>("success", HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<ProjectModel> getProject(Long projectNo) {
        log.info("projectNo : " + projectNo);
        return new ResponseEntity<>(projectService.findByNo(projectNo), HttpStatus.OK);
    }

    @GetMapping("projectLog")
    public ResponseEntity<ProjectModel> getProjectLog(Long projectNo) {
        return new ResponseEntity(projectLogRepository.findByProjectModel(new ProjectModel(projectNo)), HttpStatus.OK);
    }

    @DeleteMapping
    public ResponseEntity delete(@RequestParam Long no) {
        projectRepository.deleteById(no);
        return new ResponseEntity(HttpStatus.valueOf(200));
    } // delete()

//    @GetMapping("Test-link")
//    public ResponseEntity<String> TestLink(@RequestParam String url) {
//        Integer resCode = null;
//        String msg;
//        try {
//            resCode = new ExecuteCrawling(serverURL + "test/" + url).sendGet();
//            log.debug("TestLink: Response Code[" + resCode + "]");
//        } catch (Exception e) {
//            log.error(String.valueOf(e));
//        } //try~catch
//
//        new ExecuteCrawling();
//        if(resCode == 200) {
//            return new ResponseEntity<String>("확인되었습니다",HttpStatus.OK);
//        } else if(resCode == 400) {
//            return new ResponseEntity<String>("",HttpStatus.OK);
//        }
//    } //TestLink();

} //class