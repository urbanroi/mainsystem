package com.urban.ROI_System.controller;

import com.urban.ROI_System.model.ProductModel;
import com.urban.ROI_System.model.UserModel;
import com.urban.ROI_System.repository.ProductRepository;
import com.urban.ROI_System.service.ProductLogService;
import com.urban.ROI_System.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/rest/product")
@Slf4j
public class ProductRestController {

    @Autowired private ProductService prodService;
    @Autowired private ProductRepository productRepository;
    @Autowired private ProductLogService logService;

    @GetMapping("/date")
    public ResponseEntity<Map<String, List<?>>> getProductsByDate(@RequestParam String startDate, @RequestParam String endDate, HttpSession session) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

        LocalDateTime parsedStartDate = LocalDateTime.parse(startDate, formatter);
        LocalDateTime parsedEndDate = LocalDateTime.parse(endDate, formatter);

        UserModel user = (UserModel) session.getAttribute("loginInfo");
        if (user == null) return new ResponseEntity<>(HttpStatus.FORBIDDEN);

        Map<String, List<?>> response = new HashMap<>();
        //response.put("prods", prodService.findAllByDate(parsedStartDate, parsedEndDate, user));
        response.put("prods", prodService.findAllByLogCreatedDate(parsedStartDate, parsedEndDate, user));
        response.put("labels", logService.getCreateDateDistinct(parsedStartDate, parsedEndDate, user));

        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @PatchMapping
    public ResponseEntity updateProduct(@RequestParam ProductModel productModel) {
        productRepository.save(productModel);
        return new ResponseEntity(HttpStatus.OK);
    } //updateProduct();

    @GetMapping
    public ResponseEntity<ProductModel> selectProductModel(@RequestParam Long no) {
        return new ResponseEntity<ProductModel>(productRepository.findByNo(no),HttpStatus.OK);
    } //selectProductModel();

    @PostMapping
    public ResponseEntity<?> saveProduct(HttpSession session) {
        ProductModel product = prodService.save(
            (String)session.getAttribute("productName"),
            (String)session.getAttribute("accountId"),
            (UserModel) session.getAttribute("loginInfo")
        );

        session.setAttribute("productNo", product.getNo());

        return new ResponseEntity<>(HttpStatus.OK);
    }

    /*
    @PostMapping
    public ResponseEntity<?> saveProduct(@RequestParam String productName, HttpSession session) {
        prodService.save(productName, (UserModel) session.getAttribute("loginInfo"));

        return new ResponseEntity<>(HttpStatus.OK);
    }
     */

    @GetMapping("/search")
    public ResponseEntity<Long> findSearchCount(@RequestParam String keyword, HttpSession session) {
        UserModel userModel = (UserModel) session.getAttribute("loginInfo");
        return new ResponseEntity<>(prodService.countByUserModelAndProductName(userModel, keyword), HttpStatus.OK);
    }

    @GetMapping("/my-list")
    public ResponseEntity<List<ProductModel>> findByUserNo(HttpSession session) {
        UserModel userModel = (UserModel) session.getAttribute("loginInfo");
        return new ResponseEntity<List<ProductModel>>(prodService.findByUserNo(userModel), HttpStatus.OK);
    }

    @DeleteMapping
    public ResponseEntity deleteProduct(Long productNo){
        System.out.println(productNo);
        productRepository.deleteById(productNo);
        System.out.println("controller:"+productNo);
        return new ResponseEntity(HttpStatus.OK);
    }
}