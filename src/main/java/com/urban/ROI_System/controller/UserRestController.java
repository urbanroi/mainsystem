package com.urban.ROI_System.controller;

import com.urban.ROI_System.model.UserModel;
import com.urban.ROI_System.service.UserService;
import com.urban.ROI_System.util.MailSender;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping("/rest/user")
@Slf4j
public class UserRestController {

	@Autowired private UserService userService;
	@Autowired private MailSender mailService;
	

	@GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<List<UserModel>> getAllUsers() {
		List<UserModel> user = userService.findAll();
		return new ResponseEntity<List<UserModel>>(user, HttpStatus.OK);
	}
	
	// id check
	@GetMapping("/id-check")
	public ResponseEntity<Boolean> idCheck(@RequestParam String id) {
		return new ResponseEntity<>((userService.findById(id) != null) ? false : true, HttpStatus.OK);
	}
	
	/* Email */
	// send mail code
	@GetMapping("/send-mail-code")
	public ResponseEntity<Boolean> sendMailCode(HttpSession session, @RequestParam String email) {
		log.info(email);
		if(userService.findByEmail(email) != null) {
			return new ResponseEntity<>(false, HttpStatus.CONFLICT);
		}
		// mailCode = 10000 ~ 99999 범위의 난수
		int mailCode = (int) (Math.random() * (99999 - 10000) + 10000); 
		
		session.setAttribute("mailCode", mailCode);
		log.info("[session : " + session.getAttribute("mailCode") + "]");
		mailService.sendMail(email, mailCode); // 메일 전송
		
		return new ResponseEntity<>(true, HttpStatus.OK);
	}
	
	// check mail code
	@GetMapping("/check-mail-code")
	public ResponseEntity<Boolean> checkMailCode(HttpSession session, @RequestParam int mailCode) {
		Boolean result =
		(mailCode == (int)session.getAttribute("mailCode")) ? true : false;
	
		return  new ResponseEntity<>(result, HttpStatus.OK);
	}
	
}