package com.urban.ROI_System.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/admin/config/crawl/site-list")
@Slf4j
public class CrawlSiteListController {
    @GetMapping
    public String CrawlSiteList(Model model, HttpSession session) {
        return "crawling/crawlsitelist";
    }
}
