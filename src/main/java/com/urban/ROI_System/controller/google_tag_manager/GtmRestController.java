package com.urban.ROI_System.controller.google_tag_manager;

import com.urban.ROI_System.model.GtmTagModel;
import com.urban.ROI_System.model.ProductModel;
import com.urban.ROI_System.model.UserModel;
import com.urban.ROI_System.repository.GtmTagRepository;
import com.urban.ROI_System.service.ProductService;
import com.urban.ROI_System.service.google_tag_manager.GTMService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping("/rest/gtm")
@Slf4j
public class GtmRestController {

    @Autowired private GTMService gtmService;
    @Autowired private ProductService prodService;

    @Autowired private GtmTagRepository gtmRepo;


    @GetMapping("/set")
    public ResponseEntity<Long> setGtmAndSave(HttpSession session) {
        String randomId = Integer.toString((int) Math.ceil(Math.random() * 9999 + 1));
        Long productNo = (Long) session.getAttribute("productNo");

        ProductModel product = prodService.findByNo(productNo);

        /*
        Boolean result = gtmService.setAll(
                product,
    ((UserModel) session.getAttribute("loginInfo")).getId() + "_" +
                productNo + "_" + randomId
        );
         */

        String result = gtmService.setAll(
                product,
                ((UserModel) session.getAttribute("loginInfo")).getId() + "_" +
                productNo + "_" + randomId
        );

        if (result != null) session.setAttribute("gtmId", result);

        return new ResponseEntity<> (productNo, result != null ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }

    @PostMapping("/{productNo}")
    public ResponseEntity saveGtms(@PathVariable("productNo") Long productNo, HttpSession session) {
        String gtmId = (String) session.getAttribute("gtmId");
        if (gtmId == null) return new ResponseEntity(HttpStatus.BAD_REQUEST);

        ProductModel prod = prodService.findByNo(productNo);
        GtmTagModel tag = new GtmTagModel();
        tag.setProductModel(prod);
        tag.setGtmTag(gtmId);

        gtmRepo.save(tag);

        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/{productNo}")
    public ResponseEntity<List<GtmTagModel>> getGtms(@PathVariable(value = "productNo") Long productNo) {
        log.info("productNo: "+productNo);
        List<GtmTagModel> gtms = gtmService.getGtms(prodService.findByNo(productNo));

        return new ResponseEntity<>(gtms, gtms != null ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }

}
