package com.urban.ROI_System.controller.google_analytics;

import com.urban.ROI_System.model.ProductModel;
import com.urban.ROI_System.service.ProductService;
import com.urban.ROI_System.service.management.AccountsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@RestController
@Slf4j
@RequestMapping("/rest/ga")
public class GARestController {

    @Autowired private AccountsService accountsService;
    @Autowired private ProductService prodService;

    @GetMapping("/popupExit")
    public ResponseEntity<Boolean> popupExit(HttpSession session) {
        log.info("popupExit");

        session.removeAttribute("popupLevel");
        session.removeAttribute("GACheck");
        session.removeAttribute("productName");
        session.removeAttribute("accountId");
        session.removeAttribute("token");

        return new ResponseEntity<Boolean>(true, HttpStatus.valueOf(200));
    } // popupExit()

    @GetMapping("/popup")
    public ResponseEntity changePopupLevel(String popupLevel, HttpSession session) {
        session.setAttribute("popupLevel", popupLevel);

        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/gtag")
    public ResponseEntity<String> popupChange(HttpSession session) {
        String accountId = (String) session.getAttribute("accountId");
        ProductModel prod = prodService.findByAccountId(accountId != null ? accountId : (String)session.getAttribute("accountId"));

        session.setAttribute("popupLevel", 5);

        if (prod != null) return new ResponseEntity<>(prod.getPropertyId(), HttpStatus.OK);
        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

} //class