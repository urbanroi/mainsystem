package com.urban.ROI_System.controller.google_analytics;

import com.google.api.services.analytics.model.Account;
import com.google.api.services.analytics.model.EntityUserLink;
import com.google.api.services.analytics.model.Webproperty;
import com.urban.ROI_System.model.ProductModel;
import com.urban.ROI_System.model.UserModel;
import com.urban.ROI_System.service.ProductService;
import com.urban.ROI_System.service.management.ManagementService;
import com.urban.ROI_System.vo.Token;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.*;

@Slf4j
@RequestMapping("/rest/ga/management")
@RestController
public class ManagementRestController {

    @Autowired private ManagementService managementService;
    @Autowired private ProductService productService;

    /**
     * 액세스 토큰으로 계정 목록 조회
     * @param session
     * @return
     * @throws GeneralSecurityException
     * @throws IOException
     */
    @GetMapping("/accounts")
    public ResponseEntity<List<Account>> getAccounts(HttpSession session) throws GeneralSecurityException, IOException {
        Token token = (Token) session.getAttribute("token");
        List<Account> accounts = managementService.getAccounts(token.getAccessToken());
        List<ProductModel> products = productService.findByUserNo((UserModel) session.getAttribute("loginInfo"));

        session.setAttribute("popupLevel", 3);

        return new ResponseEntity<>(managementService.deduplication(accounts, products), accounts != null ? HttpStatus.OK : HttpStatus.FORBIDDEN);
    }

    @DeleteMapping("/account")
    public ResponseEntity deleteAccount(HttpSession session, @RequestParam String accountId) {
        return new ResponseEntity(managementService.deleteAccount(accountId) ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }

    /**
     *  서비스 계정 연동
     *  productName.length == 0 ? popupLevel 4 : 5
     * @param session
     * @param accountId
     * @param productName
     * @return
     * @throws GeneralSecurityException
     * @throws IOException
     */
    @PostMapping("/service-account")
    public ResponseEntity<EntityUserLink> connectServiceAccount(HttpSession session, @RequestParam String accountId, String productName) throws GeneralSecurityException, IOException {
        Token token = (Token) session.getAttribute("token");
        EntityUserLink userLink = managementService.createAccountUserLink(token.getAccessToken(), accountId);

        session.setAttribute("accountId", accountId);

        if (userLink != null) {
            productService.save(
                productName == null ? (String) session.getAttribute("productName") : productName,
                accountId,
                (UserModel) session.getAttribute("loginInfo")
            );

            session.setAttribute("popupLevel", productName == null ? 5 : 6);
            session.removeAttribute("productName");
        }

        return new ResponseEntity<>(userLink, userLink != null ? HttpStatus.OK : HttpStatus.FORBIDDEN);
    }

    /**
     * 속성 및 보기 생성
     * @param displayName
     * @param webSiteUrl
     * @param session
     * @return
     * @throws GeneralSecurityException
     * @throws IOException
     * @throws InterruptedException
     */
    @PostMapping("/property")
    public ResponseEntity<ProductModel> createProperty(@RequestParam String displayName, @RequestParam String webSiteUrl, HttpSession session) throws GeneralSecurityException, IOException {
        Token token = (Token) session.getAttribute("token");
        ProductModel result = managementService.addUAProperty(token.getAccessToken(), (String)session.getAttribute("accountId"), displayName, webSiteUrl);

        session.setAttribute("productNo", result.getNo());

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/property")
    public ResponseEntity<List<Webproperty>> getProperties(HttpSession session) {
        Token token = (Token) session.getAttribute("token");
        List<Webproperty> result = managementService.getProperties((String)session.getAttribute("accountId"), token.getAccessToken());

        return new ResponseEntity<>(result, result != null ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }

}
