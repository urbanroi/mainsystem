package com.urban.ROI_System.repository;

import com.urban.ROI_System.model.RecommendModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RecommendRepository extends JpaRepository<RecommendModel, Integer> {
    List<RecommendModel> findAllBySmallCategoryNoAndBigCategoryNoAndGenderAndAgeOrderByRoiDesc(Integer smallCategoryNo,Integer bigCategoryNo,Character gender,Integer age);
}