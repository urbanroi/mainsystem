package com.urban.ROI_System.repository;

import com.urban.ROI_System.model.ProjectDataModel;
import org.springframework.data.jpa.repository.JpaRepository;
public interface ProjectDataRepository extends JpaRepository<ProjectDataModel, Long> {
}
