package com.urban.ROI_System.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.urban.ROI_System.model.CrawlSiteListModel;
import java.util.List;

public interface CrawlSiteListRepository extends JpaRepository<CrawlSiteListModel, Integer> {

	public CrawlSiteListModel findByNo(Long no);
	public List<CrawlSiteListModel> findByLink(String link);
	public Boolean existsByNo(Long no);

}