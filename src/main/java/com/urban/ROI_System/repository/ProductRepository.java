package com.urban.ROI_System.repository;

import com.urban.ROI_System.model.ProductModel;
import com.urban.ROI_System.model.UserModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;

public interface ProductRepository extends JpaRepository<ProductModel, Long> {
	public ProductModel findByNo(Long no);
	public ProductModel findByAccountId(String accountId);
	public List<ProductModel> findByUserModel(UserModel userModel);
	public Page<ProductModel> findByUserModel(UserModel userModel, Pageable pageable);
	public List<ProductModel> findAllByCreateDateBetweenAndUserModelOrderByTotalSalesDesc(LocalDateTime start, LocalDateTime end, UserModel userModel);
	public Page<ProductModel> findByProductNameContainingIgnoreCaseAndUserModel(String productName, UserModel userModel, Pageable pageable);

	public Long countByUserModel(UserModel userModel);
	public Long countByProductNameContainingIgnoreCaseAndUserModel(String productName, UserModel userModel);

	boolean existsByNo(Long no);

	@Query("SELECT p FROM ProductModel p INNER JOIN ProductLogModel l ON p.no = l.productModel.no WHERE p.userModel.no = ?3 AND (l.createDate BETWEEN ?1 AND ?2)")
	public List<ProductModel> getProductByUserModelAndLogCreateDate(LocalDateTime start, LocalDateTime end, Long userNo);
} //ProductRepository(); 