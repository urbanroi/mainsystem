package com.urban.ROI_System.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.urban.ROI_System.model.UserModel;

public interface UserRepository extends JpaRepository<UserModel, Long> {

	UserModel findByNo(Long no);
	UserModel findById(String id);
	UserModel findByIdAndPw(String id, String pw);
	UserModel findByEmail(String email);

}
