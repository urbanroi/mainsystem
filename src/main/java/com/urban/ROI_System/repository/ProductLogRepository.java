package com.urban.ROI_System.repository;

import com.urban.ROI_System.model.ProductModel;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import com.urban.ROI_System.model.ProductLogModel;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;

public interface ProductLogRepository extends JpaRepository<ProductLogModel, Long> {

	ProductLogModel findByNo(Long no);
	List<ProductLogModel> findByProductModel(ProductModel productModel);
	List<ProductLogModel> findAllByCreateDateBetweenAndProductModel(LocalDateTime start, LocalDateTime end, ProductModel product);
	ProductLogModel findFirstByProductModelOrderByCreateDateDesc(ProductModel productModel);

	// Dashboard Main Chart 라벨 조회
	@Query("SELECT DISTINCT DATE_FORMAT(l.createDate, '%Y-%m-%d') FROM ProductLogModel l INNER JOIN ProductModel p ON p.no = l.productModel.no WHERE p.userModel.no =?1 AND (l.createDate BETWEEN ?2 AND ?3)")
	List<String> getCreateDateDistinct(Long userNo, LocalDateTime start, LocalDateTime end);

}