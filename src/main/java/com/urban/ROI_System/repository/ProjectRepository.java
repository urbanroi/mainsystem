package com.urban.ROI_System.repository;

import com.urban.ROI_System.model.ProductModel;
import com.urban.ROI_System.model.ProjectLogDTO;
import com.urban.ROI_System.model.ProjectLogModel;
import com.urban.ROI_System.model.ProjectModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;

@Repository
public interface ProjectRepository extends JpaRepository<ProjectModel, Long> {

    ProjectModel findByNo(Long no);
    ProjectModel findByProjectName(String projectName);

    Long countByProductModel(ProductModel productModel);

    List<ProjectModel> findByProductModel(ProductModel productModel);

    // 파라미터와 함께 조회할 때는 PageRequest 대신 Pageable 사용
    Page<ProjectModel> findAByProductModel(ProductModel productModel, Pageable pageable);

    @Modifying
    @Transactional
    @Query("UPDATE ProjectModel p SET p.lastCollectionDate = NOW() WHERE p.no = ?1")
    void updateLastCollectionDate(Long no);

//    @Query(value = "SELECT SUM(p.direct) as direct, SUM(p.indirect) as indirect, SUM(p.brandValue) as brand_value, SUM(p.total) as total FROM ProjectLogModel p GROUP BY p.projectModel.no HAVING p.projectModel.no=:no",nativeQuery = true)
//    ProjectLogModel getProjectReport(@Param("no") Long no);

    @Query("SELECT SUM(p.property) FROM ProjectModel p WHERE p.productModel.no =:no")
    Long getTotalPropertyByProductNo(@Param("no") Long no);

    //List<ProjectModel> findByProductNo(Long no);
}