package com.urban.ROI_System.repository;

import com.urban.ROI_System.model.BigCategoryModel;
import com.urban.ROI_System.model.SmallCategoryModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SmallCategoryRepository extends JpaRepository<SmallCategoryModel, Integer> {

    SmallCategoryModel findByNo(int no);
    List<SmallCategoryModel> findByBigCategoryModel (BigCategoryModel b);
}
