package com.urban.ROI_System.repository;

import com.urban.ROI_System.model.GtmCSSSelectorModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GtmCSSSelectorRepository extends JpaRepository<GtmCSSSelectorModel, Long> {
    public GtmCSSSelectorModel findByName(String name);
}
