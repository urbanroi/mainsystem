package com.urban.ROI_System.repository;

import com.urban.ROI_System.model.BigCategoryDTO;
import com.urban.ROI_System.model.BigCategoryModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BigCategoryRepository extends JpaRepository<BigCategoryModel, Integer> {

    BigCategoryModel findByNo(int no);
    List<BigCategoryDTO> findAllBy();

}
