package com.urban.ROI_System.repository;

import com.urban.ROI_System.model.GtmTagModel;
import com.urban.ROI_System.model.ProductModel;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface GtmTagRepository extends JpaRepository<GtmTagModel, Long> {

    public List<GtmTagModel> findByProductModel(ProductModel productModel);

}
