package com.urban.ROI_System.repository;

import com.urban.ROI_System.model.ProjectLogDTO;
import com.urban.ROI_System.model.ProjectLogModel;
import com.urban.ROI_System.model.ProjectModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjectLogRepository  extends JpaRepository<ProjectLogModel, Long> {

    List<ProjectLogModel> findByProjectModel(ProjectModel projectModel);
    ProjectLogModel findFirstByProjectModelOrderByCreateDateDesc(ProjectModel projectModel);

    /*
    @Query("SELECT l.createDate AS createDate, SUM(l.direct) AS direct, SUM(l.indirect) AS indirect, (SUM(l.total) / SUM(p.property)*10000) as ROI FROM ProjectLogModel l INNER JOIN ProjectModel p ON p.no = l.projectModel.no  WHERE p.productModel.no = :no GROUP BY l.createDate")
    List<ProjectLogDTO> getProject(@Param("no") Long no);
    */
    @Query(value = "SELECT l.create_date AS createDate, SUM(l.direct) AS direct, SUM(l.indirect) AS indirect, (SUM(l.total) / SUM(p.property))*10000 as ROI FROM project_log l INNER JOIN project p ON p.no = l.project_no WHERE p.product_no = :no GROUP BY l.create_date",
    nativeQuery = true)
    List<ProjectLogDTO> getProject(@Param("no") Long no);


}

