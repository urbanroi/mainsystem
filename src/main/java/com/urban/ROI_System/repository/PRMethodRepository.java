package com.urban.ROI_System.repository;

import com.urban.ROI_System.model.CrawlSiteListModel;
import com.urban.ROI_System.model.PRMethodModel;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface PRMethodRepository extends JpaRepository<PRMethodModel, Long> {
    public List<PRMethodModel> findByCrawlSiteListModel(CrawlSiteListModel crawlSiteListModel);
    public PRMethodModel findByNo(Long no);
    public boolean existsByNo(Long no);
}
