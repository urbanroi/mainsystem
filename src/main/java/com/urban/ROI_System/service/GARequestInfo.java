package com.urban.ROI_System.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.urban.ROI_System.service.management.AccountsService;
import com.urban.ROI_System.util.BeanUtil;
import com.urban.ROI_System.util.HttpRequestUtil;
import com.urban.ROI_System.util.ServerValue;
import com.urban.ROI_System.vo.GA_AccountInfo;
import com.urban.ROI_System.vo.HttpResponseVO;
import com.urban.ROI_System.vo.Token;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Date;

@Slf4j
@Service
public class GARequestInfo {

    public Token getToken(String code) throws Exception {
            String query = "code=" + code
                + "&client_id=" + ServerValue.get().getClientID()
                + "&client_secret=" + ServerValue.get().getClientPW()
                + "&redirect_uri=" + ServerValue.get().getRedirectURI()
                + "&grant_type=authorization_code";

            String tokenJson;

            tokenJson = new HttpRequestUtil("https://accounts.google.com/o/oauth2/token").sendPost(query).getBody(); //token 요청
            Token jsonToken = BeanUtil.getObjectMapper().readValue(tokenJson, Token.class); //json 파싱
            return jsonToken;
    } //requestToken();

    /**
     * GA 계정 생성을 위한 Ticket 생성
     * @param name 생성할 제품이름(GA 계정이름)
     * @param session
     * @return
     * @throws IOException
     */
    public String getTicket(String name, HttpSession session) throws IOException, InterruptedException {
        Token token = (Token)session.getAttribute("token");
        Date date = new Date((System.currentTimeMillis() * 1000) + token.getExpiresIn()); //토큰 만료 시간 발급 시간부터 1시간

        String ticket="";
        ticket = BeanUtil.getBean(AccountsService.class).getAccountTicket(token.getAccessToken(), date, name);
        log.info("ticket : " + ticket);

        return ticket;
    } //requestTicket();

    /**
     * GA 사용자 정보 조회
     * @param token
     * @return GA_AccountInfo
     * @throws IOException
     */
    public GA_AccountInfo getGaUserAccount(Token token) throws IOException {
            HttpResponseVO ret = new HttpRequestUtil("https://www.googleapis.com/oauth2/v1/userinfo?access_token="+token.getAccessToken()).sendGet();
            ObjectMapper mapper = BeanUtil.getObjectMapper();
            GA_AccountInfo info = mapper.readValue(ret.getBody(), GA_AccountInfo.class);
        return info;
    }

} // GAService();