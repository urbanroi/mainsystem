package com.urban.ROI_System.service;

import com.urban.ROI_System.model.ProductLogModel;
import com.urban.ROI_System.model.ProductModel;
import com.urban.ROI_System.model.UserModel;
import com.urban.ROI_System.repository.ProductLogRepository;
import com.urban.ROI_System.repository.ProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.jni.Local;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@Service
public class ProductLogService {
    @Autowired private ProductLogRepository logRepo;
    @Autowired private ProductRepository prodRepo;

    public List<ProductLogModel> findByProductNo(Long productNo) {
        ProductModel prod = prodRepo.findByNo(productNo);
        if (prod == null) return null;

        List<ProductLogModel> list = logRepo.findByProductModel(prod);
        if (list.size() == 0) return null;

        return list;
    }

    public ProductLogModel save(ProductLogModel productLog) {
        return logRepo.save(productLog);
    }

    public ProductLogModel getLastLog(ProductModel product) { return logRepo.findFirstByProductModelOrderByCreateDateDesc(product); }

    public List<String> getCreateDateDistinct(LocalDateTime start, LocalDateTime end, UserModel userModel) { return logRepo.getCreateDateDistinct(userModel.getNo(), start, end); }

}
