package com.urban.ROI_System.service;

import com.urban.ROI_System.util.HttpRequestUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.net.URLEncoder;

@Service
@Slf4j
public class LinkValidationService {

    @Value("${linkTestServer.url}")
    String serverUrl;

    public int send(String link) {
        try {
            String encodeStr = URLEncoder.encode(link,"UTF-8");
            HttpRequestUtil exe = new HttpRequestUtil(serverUrl + encodeStr);

            return exe.sendGet().getStatus();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    } // send();
} //class