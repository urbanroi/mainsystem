package com.urban.ROI_System.service.management;

import com.google.api.services.analytics.model.Account;
import com.google.api.services.analytics.model.EntityUserLink;
import com.google.api.services.analytics.model.Profile;
import com.google.api.services.analytics.model.Webproperty;
import com.urban.ROI_System.google_api.management.Accounts;
import com.urban.ROI_System.google_api.management.Managements;
import com.urban.ROI_System.model.ProductModel;
import com.urban.ROI_System.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class ManagementService {

    @Autowired private ProductService prodService;

    /**
     * 계정 목록 조회
     * @param tokenValue : 액세스 토큰
     * @return List<Account>
     * @throws GeneralSecurityException
     * @throws IOException
     */
    public List<Account> getAccounts(String tokenValue) throws GeneralSecurityException, IOException {
        return new Managements(tokenValue).getAccounts();
    }

    /**
     * 서비스계정과 특정 계정 연동
     * @param tokenValue : 액세스 토큰
     * @param accountId : 계정 고유 번호
     * @return EntityUserLink
     * @throws GeneralSecurityException
     * @throws IOException
     */
    public EntityUserLink createAccountUserLink(String tokenValue, String accountId) throws GeneralSecurityException, IOException {
        return new Managements(tokenValue).createAccountUserLink(accountId);
    }

    /**
     * 속성 및 보기 생성
     * @param tokenValue : 액세스 토큰
     * @param accountId : 계정 고유 번호
     * @param displayName : 속성 이름
     * @param websiteUrl : 자사 쇼핑몰 URL
     * @return Boolean
     * @throws GeneralSecurityException
     * @throws IOException
     */
    public ProductModel addUAProperty(String tokenValue, String accountId, String displayName, String websiteUrl) throws GeneralSecurityException, IOException {
        Managements analytics = new Managements(tokenValue);
        List<Object> response = analytics.create(accountId, websiteUrl, displayName);
        Webproperty property = (Webproperty) response.get(0);
        Profile profile = (Profile) response.get(1);

        if (response == null) return null;
        return prodService.updatePropertyId(accountId, property.getId(), profile.getId());
    }

    /**
     * List<Account> 에서 List<ProductModel> 중복 제거
     * @param accounts
     * @param products
     * @return List<Account>
     */
    public ArrayList<Account> deduplication(List<Account> accounts, List<ProductModel> products) {
        Map<String, Account> map = new HashMap<>();
        ArrayList<Account> result = new ArrayList<>();

        for (Account a : accounts) map.put(a.getId(), a);
        for (ProductModel productModel : products) map.remove(productModel.getAccountId());
        map.forEach((k, v) -> result.add(v));

        return result;
    }

    public Boolean deleteAccount(String accountId) {
        try {
            return new Accounts().delete(accountId);
        } catch (IOException e) {
            log.error(e.getMessage());
            return false;
        }
    }

    public List<Webproperty> getProperties(String accountId, String token) {
        try {
            return new Managements(token).getProperties(accountId);
        } catch (GeneralSecurityException | IOException e) {
            log.error(e.getMessage());
            return null;
        }
    }
}
