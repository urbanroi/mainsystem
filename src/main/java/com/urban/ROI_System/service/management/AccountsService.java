package com.urban.ROI_System.service.management;

import com.google.analytics.admin.v1alpha.Account;
import com.google.auth.oauth2.AccessToken;
import com.google.auth.oauth2.OAuth2Credentials;
import com.urban.ROI_System.google_api.management.Accounts;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class AccountsService {

    /**
     * AccountTicket 가져오기
     * @param token : Access Token
     * @param expirationTime : 토큰 만료 시간 (expires_in)
     * @param displayName : 사용자 입력 계정이름
     * @return String accountToken
     */
    public String getAccountTicket(String token, Date expirationTime, String displayName) throws IOException, InterruptedException {
        AccessToken accessToken = new AccessToken(token, expirationTime);
        OAuth2Credentials credentials = OAuth2Credentials.create(accessToken);
        Accounts analytics = new Accounts();
        String result = analytics.getAccountTicket(credentials, displayName);

        analytics.terminate();
        return result;
    }

    /**
     * 계정 고유번호 생성 여부 체크
     * @param token : Access Token
     * @param expirationTime : 토큰 만료 시간
     * @param accountId : 체크할 계정 고유 번호
     * @return Boolean
     * @throws IOException
     */
    public Boolean checkAccount(String token, Date expirationTime, String accountId) throws IOException, InterruptedException {
        AccessToken accessToken = new AccessToken(token, expirationTime);
        OAuth2Credentials credentials = OAuth2Credentials.create(accessToken);
        Accounts analytics = new Accounts();
        List<Account> list = analytics.getListByAccessToken(credentials);
        analytics.terminate();

        if (list == null) return false;

        for(Account account : list) {
            if (account.getName().split("/")[1].equals(accountId)) return true;
        }
        return false;
    }

    /**
     * 계정 서비스계정과 연동 여부 체크
     * @param accountId : 체크할 계정 고유 번호
     * @return Boolean
     * @throws IOException
     */
    public Boolean checkServiceAccount(String accountId) throws IOException, InterruptedException {
        log.info("checkServiceAccount");
        log.info("accountId : " + accountId);

        Accounts analytics = new Accounts();
        List<Account> result = (List<Account>) analytics.getList();

        //result.forEach(i->log.info(i.getName()));
        analytics.terminate();

        for (Account account : result)
            if (account.getName().split("/")[1].equals(accountId)) return true;

        return false;
    }

    /**
     * 특정계정 조회
     * @param accountId : 계정 고유 번호
     * @return Account
     * @throws IOException
     */
    public Account getAccount(String accountId) throws IOException {
        return (Account) new Accounts().get(accountId);
    }
}