package com.urban.ROI_System.service.reporting;

import com.google.api.services.analyticsreporting.v4.model.ReportData;
import com.urban.ROI_System.google_api.reporting.Reporting;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.GeneralSecurityException;

@Service
public class ReportingService {

    public String getSingleDayData(String viewId) {
        Reporting service = new Reporting();
        try {
            service.initialize(viewId);
            return service.getSingleDayRevenueData();
        } catch (IOException | GeneralSecurityException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }
}
