package com.urban.ROI_System.service;

import com.google.api.services.analytics.model.McfData;
import com.google.api.services.analyticsreporting.v4.model.ReportRow;
import com.urban.ROI_System.exception.UTMUnknownTypeException;
import com.urban.ROI_System.google_api.reporting.Reporting;
import com.urban.ROI_System.model.*;
import com.urban.ROI_System.repository.ProductRepository;
import com.urban.ROI_System.repository.ProjectLogRepository;
import com.urban.ROI_System.repository.ProjectRepository;
import com.urban.ROI_System.repository.RecommendRepository;
import com.urban.ROI_System.service.reporting.ReportingService;
import com.urban.ROI_System.util.HttpRequestUtil;
import com.urban.ROI_System.util.UTMBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class SchedulingService {

    @Value("${crawlServer.url}")
    private String serverUrl;
    private Reporting reporting = new Reporting();
    @Autowired
    private ProductRepository pr;
    @Autowired
    private ReportingService rs;
    @Autowired
    private DataUpdateService dus;
    @Autowired
    private ProjectLogRepository projectLogRepository;
    @Autowired
    private ProjectRepository projectRepo;
    @Autowired
    private ProjectService projectService;
    @Autowired
    private RecommendRepository recommendRepository;
    @Autowired
    private ProductLogService productLogService;
    @Autowired
    private ProductService productService;

    @Scheduled(cron = "0 0 3 * * *") //(초 분 시 일 월 요일)
    public void run() throws Exception {

        new HttpRequestUtil(serverUrl);

        UTMBuilder utmBuilder = new UTMBuilder();
        String[] arrStr;
        for (ProductModel productModel : pr.findAll()) {

            reporting.initialize(productModel.getViewId());


            long 총매출 = Long.parseLong(reporting.getSingleDayRevenueData());

            log.info("--------------------");
            log.info("제품번호 ["+productModel.getNo()+"]");

            if (총매출 == 0) {
                log.info("총매출없음");
                continue;
            }

            List<ProjectLogModel> projectLogList = new ArrayList<>();
            long totalProperty = 0L;
            long totalDirect = 0L;
            long totalIndirect = 0L;
            long totalBrandValue = 0L;
            double totalROI;

            Map<Long, Long> projectDirectMap = new HashMap<>();
            Map<Long, Long> projectInDirectMap = new HashMap<>();

            //직접기여
            try {
                log.info("직접기여");

                for (ReportRow row : reporting.getDirectList()) {
                    String value = row.getMetrics().get(0).get("values").toString();
                    value = value.substring(1, value.length()-1);

                    log.info(row.getDimensions().get(1));
                    log.info(value);
                    projectDirectMap.put(Long.parseLong(row.getDimensions().get(1)), Long.parseLong(value));

                }
            } catch (NullPointerException|NumberFormatException e) {
                try { indirectListFunc(reporting.getIndirectList(), projectInDirectMap);  log.info("간접기여");}
                catch (NumberFormatException | NullPointerException ee) { continue; }
            }

            try { indirectListFunc(reporting.getIndirectList(), projectInDirectMap); log.info("직접기여 있는 간접기여");}
            catch (NullPointerException | NumberFormatException e) { log.info("간접기여 null");  }

            Long 광고직 = Long.parseLong(reporting.getSingleDaySourceMediumData("", "ads"));
            Long 홍보직 = Long.parseLong(reporting.getSingleDaySourceMediumData("", "promotion"));
            Long 홍광직총 = 광고직 + 홍보직;

            Double 홍광간총 = reporting.getSingleDayMcfDataAssistedValue();

            log.info("제품 번호: " + productModel.getNo());
            log.info("총매출: " + 총매출);
            log.info("홍광직총: " + 홍광직총);
            log.info("홍광간총: " + 홍광간총);


            Long 총브가 = 총매출 - (홍광직총 + Long.parseLong((Math.round(홍광간총) + "")));
            if (총매출 == (홍광직총)) 총브가 = 0L;
            log.info("총브가:" + 총브가);
            log.info("--------------------");

            List<ProjectModel> projectList = projectRepo.findByProductModel(productModel);
/*
            try {

            } catch (NullPointerException e4) {
                log.info("간접기여 없음");
                for (ProjectModel projectModel :projectList) {
                    ProjectLogModel j = new ProjectLogModel();
                    j.setProjectModel(projectModel);
                    j.setTotal(Long.parseLong("0"));
                    j.setDirect(Long.parseLong("0"));
                    j.setIndirect(Long.parseLong("0"));
                    j.setBrandValue(Long.parseLong("0"));
                    projectLogRepository.save(j);
                }
//                continue;
            }
*/

            for(ProjectModel projectModel : projectList) {

                ProjectLogModel projectLogModel = new ProjectLogModel();
                totalProperty += projectModel.getProperty();
                long directValue = projectDirectMap.get(projectModel.getNo()) == null ? 0L : projectDirectMap.get(projectModel.getNo());
                long inDirectValue = projectInDirectMap.get(projectModel.getNo()) == null ? 0L : projectInDirectMap.get(projectModel.getNo());
                totalDirect += directValue;
                totalIndirect += inDirectValue;
                projectLogModel.setDirect(directValue);
                projectLogModel.setIndirect(inDirectValue);
                projectLogModel.setProjectModel(projectModel);
                projectLogList.add(projectLogModel);
                projectModel.setSales(directValue+inDirectValue);
                projectRepo.save(projectModel);
            }

            try {
                totalBrandValue = 총매출 - (totalDirect + totalIndirect);
                totalROI = Double.parseDouble((총매출 / totalProperty + (총매출 % totalProperty)) + "");
            } catch (ArithmeticException e) {
                totalROI = 0.0;
            }

            //제품 log 생성
            try {
                // 24시간 내에 기록된 로그가 있는 경우 해당 로그를 업데이트
                try {
                    ProductLogModel lastLog = productLogService.getLastLog(productModel);

                    if (lastLog.getCreateDate().toString().split("T")[0].equals(LocalDateTime.now().toString().split("T")[0])) {
                        log.info("기존로그 업데이트");
                        lastLog.setTotalSales(총매출);
                        lastLog.setTotalProperty(totalProperty);
                        lastLog.setTotalRoi(totalROI);

                        productLogService.save(lastLog);
                    } else {
                        log.info("새 로그 생성");
                        ProductLogModel productLog =
                                ProductLogModel.builder()
                                        .productModel(productModel)
                                        .totalSales(총매출)
                                        .totalProperty(totalProperty)
                                        .totalRoi(totalROI)
                                        .build();
                        productLogService.save(productLog);
                    }
                    // 로그 없는경우
                } catch (NullPointerException e) {
                    log.info("새 로그 생성");
                    ProductLogModel productLog =
                            ProductLogModel.builder()
                                    .productModel(productModel)
                                    .totalSales(총매출)
                                    .totalProperty(totalProperty)
                                    .totalRoi(totalROI)
                                    .build();
                    productLogService.save(productLog);
                }

                productModel.setTotalSales(총매출);
                productModel.setTotalProperty(totalProperty);
                productModel.setTotalRoi(totalROI);

                productService.save(productModel);

            } catch (Exception e) {
                e.printStackTrace();
            }

            for (ProjectLogModel projectLogModel : projectLogList) {

                long direct = projectLogModel.getDirect();
                long inDirect = projectLogModel.getIndirect();

                log.info("프로젝트 로그 생성");
                try {
                    projectLogModel.setBrandValue(totalBrandValue / (총매출 / (direct + inDirect)));
                 } catch (ArithmeticException e) {
                    projectLogModel.setBrandValue(0L);
                }
                log.info("로그 세이브");
                projectLogModel.setTotal(projectLogModel.getBrandValue() + direct + inDirect);
                projectLogRepository.save(projectLogModel);
            }
        }

        dus.deleteData();

        for (ProjectModel pm : projectRepo.findAll()) {
            if (LocalDateTime.now().toString().split("T")[0].equals(pm.getSimpleProjectEndDate())) {
                try {
                    log.info("recommend insert");
                    String site = pm.getCrawlSiteListModel().getName();
                    ProjectLogModel firstLog = projectLogRepository.findFirstByProjectModelOrderByCreateDateDesc(pm);

                    recommendRepository.save(
                            new RecommendModel()
                                    .builder()
                                    .age(pm.getAge())
                                    .bigCategoryNo(pm.getBigCategoryModel().getNo())
                                    .gender(pm.getGender())
                                    .site(site.equals("직접 입력") ? pm.getUtm().split("source=")[1].split("&")[0] : site)
                                    .smallCategoryNo(pm.getSmallCategoryModel().getNo())
                                    .type(utmBuilder.isPromotion(pm.getUtm()) ? "광고" : "홍보")
                                    .roi(Double.parseDouble((firstLog == null ? 0 : firstLog.getTotal()) / (pm.getProperty() == null ? 0 : pm.getProperty()) + ""))
                                    .build()
                    );
                } catch (UTMUnknownTypeException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    } //run();

    public void indirectListFunc(List<List<McfData.Rows>> list, Map<Long, Long> projectInDirectMap) throws NullPointerException, NumberFormatException {
        for (List<McfData.Rows> row : list) {
            log.info(row.toString());
            Long no;
            try{
                no = Long.parseLong(row.get(0).get("primitiveValue")+"");
            } catch (NumberFormatException e) {
                continue;
            }
            projectInDirectMap.put(no, Long.parseLong(row.get(2).get("primitiveValue").toString().split("\\.")[0]));
        }
    }


}//scheduler();