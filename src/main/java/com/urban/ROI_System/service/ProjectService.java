package com.urban.ROI_System.service;

import com.urban.ROI_System.model.*;
import com.urban.ROI_System.repository.*;
import com.urban.ROI_System.util.UTMBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class ProjectService {
    @Autowired
    private ProjectRepository projectRepo;
    @Autowired
    private ProductRepository productRepo;
    @Autowired
    private CrawlSiteListRepository siteRepo;
    @Autowired
    private ProjectLogRepository plr;
    @Autowired
    private PRMethodRepository methodRepo;
    @Autowired
    private BigCategoryRepository bigCategoryRepository;
    @Autowired
    private SmallCategoryRepository smallCategoryRepository;

    public boolean projectNameDuplicateCheck(String projectName, Long productNo) {
        ProductModel productModel = productRepo.findByNo(productNo);
        for (ProjectModel projectModel : productModel.getProjectModelList()) {
            if(projectModel.getProjectName().equals(projectName)) {
                return true;
            }
        }
        return false;
    }

    public void updateLastCollectionDate(Long no) {
        projectRepo.updateLastCollectionDate(no);
    }

    public Long getTotalPropertyByProductNo(Long no) {
        return projectRepo.getTotalPropertyByProductNo(no);
    }

    public ProjectModel findByNo(Long no) {
        return projectRepo.findByNo(no);
    }

    public List<ProjectModel> findByProductNo(Long productNo) {
        return projectRepo.findByProductModel(productRepo.findByNo(productNo));
    }

    public ProjectLogModel getLastProjectLog(ProjectModel projectModel) {
        return plr.findFirstByProjectModelOrderByCreateDateDesc(projectModel);
    }

    public Long count(Long productNo) {
        ProductModel prod = productRepo.findByNo(productNo);
        if (prod == null) return 0L;

        return projectRepo.countByProductModel(prod);
    }

    public List<ProjectModel> findByPage(int page, int size, Long productNo) {
        PageRequest pageRequest = PageRequest.of(page - 1, size, Sort.by("no").descending());
        ProductModel prod = productRepo.findByNo(productNo);
        if (prod == null) return null;

        List<ProjectModel> list = projectRepo.findAByProductModel(prod, pageRequest).getContent();
        if (list.size() == 0) return null;

        return list;
    }

    public void saveNoArgs(ProjectModel projectModel) {
        if (projectModel != null) projectRepo.save(projectModel);
    }

    public void save(ProjectModel projectModel, String method) {
        Long productNo = projectModel.getProductModel().getNo();
        Long siteNo = projectModel.getCrawlSiteListModel().getNo();
        String siteName = projectModel.getCrawlSiteListModel().getName();
        if (siteName == null) siteName = siteRepo.findByNo(siteNo).getName();


        projectModel.setCrawlSiteListModel(siteRepo.findByNo(siteNo));
        projectModel.setLinkValidity(true);
        projectModel.setProductModel(productRepo.findByNo(productNo));
        projectModel.setCrawlSiteListModel(siteRepo.findByNo(siteNo));
        //projectModel.setPrMethodModel(methodRepo.findByNo(prMethodNo));

        ProjectModel newProjectModel = projectRepo.save(projectModel);
        UTMBuilder utmBuilder = new UTMBuilder();
        String utm = utmBuilder.makeUTM(projectModel, siteName, method);
        newProjectModel.setUtm(utm);
        projectRepo.save(newProjectModel);
    }

    public void updateProject(ProjectModel project) {
        ProjectModel before = projectRepo.findByNo(project.getNo());

        if (project.getProjectName() != null) before.setProjectName(project.getProjectName());
        if (project.getLink() != null) before.setLink(project.getLink());
        if (project.getProperty() != null) before.setProperty(project.getProperty());
        if (project.getProjectStartDate() != null) before.setProjectStartDate(project.getProjectStartDate());
        if (project.getProjectEndDate() != null) before.setProjectEndDate(project.getProjectEndDate());

        before.setBigCategoryModel(bigCategoryRepository.findByNo(project.getBigCategoryModel().getNo()));
        before.setSmallCategoryModel(smallCategoryRepository.findByNo(project.getSmallCategoryModel().getNo()));
        before.setAge(project.getAge());
        before.setGender(project.getGender());

        UTMBuilder utmBuilder = new UTMBuilder();
        before.setUtm(utmBuilder.updateUTM(before));

        projectRepo.save(before);
    }

} //class
