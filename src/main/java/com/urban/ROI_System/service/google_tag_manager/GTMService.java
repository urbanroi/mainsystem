package com.urban.ROI_System.service.google_tag_manager;

import com.google.api.services.tagmanager.model.Container;
import com.google.api.services.tagmanager.model.Folder;
import com.google.api.services.tagmanager.model.Parameter;
import com.google.api.services.tagmanager.model.Trigger;
import com.google.errorprone.annotations.concurrent.LazyInit;
import com.urban.ROI_System.model.GtmCSSSelectorModel;
import com.urban.ROI_System.model.GtmTagModel;
import com.urban.ROI_System.model.ProductModel;
import com.urban.ROI_System.repository.GtmCSSSelectorRepository;
import com.urban.ROI_System.repository.GtmTagRepository;
import com.urban.ROI_System.repository.ProductRepository;
import com.urban.ROI_System.util.BeanUtil;
import com.urban.ROI_System.util.ServerValue;
import com.urban.ROI_System.vo.ServerValueVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;
import com.urban.ROI_System.google_api.google_tag_manager.GTM;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class GTMService {

    @Autowired
    private GtmCSSSelectorRepository gtmCssRepo;
    @Autowired
    private GtmTagRepository gtmTagRepo;

    @Value("${analytics.gtmAccount}")
    private String accountId;

    public List<GtmTagModel> getGtms(ProductModel product) {
        return gtmTagRepo.findByProductModel(product);
    }


    //public Boolean setCafe24GTM(String accountId, String uaPropertyId, String containerName, String folderName) throws InterruptedException {
    public Boolean setCafe24GTM(String uaPropertyId, String containerId, String folderName) throws InterruptedException {
        log.info("[create cafe24 GTM]");
        GTM service;

        try {
            service = new GTM();
        } catch (GeneralSecurityException | IOException e) {
            return false;
        }

        try {
            //Container container = service.createContainer(accountId, containerName);
            //service.setParentPath(accountId, container.getContainerId());
            service.setParentPath(accountId, containerId);

            Folder folder = service.createFolder(folderName);
            service.setFolderId(folder.getFolderId());

            //service.createVariable("[" + folderName + "] GA TRACKING ID", "gas", "trackingId", uaPropertyId);
            service.createVariable("GA TRACKING ID", "gas", "trackingId", uaPropertyId);
            // create variable
            /*
            String[][] varParam = {
                    {"["+ folderName +"] 상품 상세 보기 - 상품 배열", "function() {var productInfo = [{'id': iProductNo,'name': product_name,'price': product_sale_price,'category': iCategoryNo}];return productInfo;}"},
                    {"["+ folderName +"] 장바구니 보기 - 상품 배열", "function() {if (aBasketProductData.length > 0) {var source = aBasketProductData;var productInfo = []; for(var i = 0 ; i < source.length ; i++) { var productId = source[i].product_no; var productName = source[i].product_name; var productPrice = source[i].product_sale_price; var productQty = source[i].quantity; var productOpt = source[i].opt_str; var productCate = source[i].main_cate_no; productInfo.push({ 'id': productId, 'name': productName, 'price': productPrice, 'variant': productOpt, 'quantity': productQty, 'category': productCate});} return productInfo; }}"},
                    {"["+ folderName +"] 주문서 작성 - 상품 배열", "function() {var source = aBasketProductOrderData;var productInfo = [];for(var i = 0 ; i < source.length ; i++) {var productId = source[i].product_no;var productPrice = source[i].product_sale_price;var productQty = source[i].quantity;var productCate = source[i].main_cate_no; productInfo.push({'id': productId, 'price': productPrice,'quantity': productQty,'category': productCate});} return productInfo; }"},
                    {"["+ folderName +"] 주문 완료 - 주문 번호", "function() { var source = EC_FRONT_EXTERNAL_SCRIPT_VARIABLE_DATA; var orderId = source.order_id; return orderId; }"},
                    {"["+ folderName +"] 주문 완료 - 결제 금액", "function() {var source = EC_FRONT_EXTERNAL_SCRIPT_VARIABLE_DATA; var revenue = source.payed_amount; return revenue;}"},
                    {"["+ folderName +"] 주문 완료 - 배송비", "function() { var source = EC_FRONT_EXTERNAL_SCRIPT_VARIABLE_DATA; var shippingFee = source.total_basic_ship_fee; return shippingFee;}"},
                    {"["+ folderName +"] 주문 완료 - 상품 배열", "function() {var source = EC_FRONT_EXTERNAL_SCRIPT_VARIABLE_DATA.order_product;var productInfo = [];for(var i = 0 ; i < source.length ; i++) {var productId = source[i].product_no; var productName = source[i].product_name;var productPrice = source[i].product_price;var productQty = source[i].quantity;var productCate = source[i].category_no_2;productInfo.push({'id': productId, 'name': productName, 'price': productPrice,'category': productCate,'quantity': productQty});} return productInfo; }"}
            };
             */
            String[][] varParam = {
                    {"상품 상세 보기 - 상품 배열", "function() {var productInfo = [{'id': iProductNo,'name': product_name,'price': product_sale_price,'category': iCategoryNo}];return productInfo;}"},
                    {"장바구니 보기 - 상품 배열", "function() {if (aBasketProductData.length > 0) {var source = aBasketProductData;var productInfo = []; for(var i = 0 ; i < source.length ; i++) { var productId = source[i].product_no; var productName = source[i].product_name; var productPrice = source[i].product_sale_price; var productQty = source[i].quantity; var productOpt = source[i].opt_str; var productCate = source[i].main_cate_no; productInfo.push({ 'id': productId, 'name': productName, 'price': productPrice, 'variant': productOpt, 'quantity': productQty, 'category': productCate});} return productInfo; }}"},
                    {"주문서 작성 - 상품 배열", "function() {var source = aBasketProductOrderData;var productInfo = [];for(var i = 0 ; i < source.length ; i++) {var productId = source[i].product_no;var productPrice = source[i].product_sale_price;var productQty = source[i].quantity;var productCate = source[i].main_cate_no; productInfo.push({'id': productId, 'price': productPrice,'quantity': productQty,'category': productCate});} return productInfo; }"},
                    {"주문 완료 - 주문 번호", "function() { var source = EC_FRONT_EXTERNAL_SCRIPT_VARIABLE_DATA; var orderId = source.order_id; return orderId; }"},
                    {"주문 완료 - 결제 금액", "function() {var source = EC_FRONT_EXTERNAL_SCRIPT_VARIABLE_DATA; var revenue = source.payed_amount; return revenue;}"},
                    {"주문 완료 - 배송비", "function() { var source = EC_FRONT_EXTERNAL_SCRIPT_VARIABLE_DATA; var shippingFee = source.total_basic_ship_fee; return shippingFee;}"},
                    {"주문 완료 - 상품 배열", "function() {var source = EC_FRONT_EXTERNAL_SCRIPT_VARIABLE_DATA.order_product;var productInfo = [];for(var i = 0 ; i < source.length ; i++) {var productId = source[i].product_no; var productName = source[i].product_name;var productPrice = source[i].product_price;var productQty = source[i].quantity;var productCate = source[i].category_no_2;productInfo.push({'id': productId, 'name': productName, 'price': productPrice,'category': productCate,'quantity': productQty});} return productInfo; }"}
            };

            for (String[] param : varParam) service.createVariable(param[0], "jsm", "javascript", param[1]);

            //System.out.println("wait");
            //TimeUnit.MINUTES.sleep(1);
            //System.out.println("restart");

            // create ua trigger & tag
            /*
            String[][] eventTrigger = {
                    {"["+ folderName +"] 이벤트 - 상품 상세 보기", "detail"},
                    {"["+ folderName +"] 이벤트 - 장바구니", "viewCart"},
                    {"["+ folderName +"] 이벤트 - 주문서 작성", "checkout"},
                    {"["+ folderName +"] 이벤트 - 주문 완료", "purchase"}
            };
            String[] uaTag = {
                    "["+ folderName +"] GA - Event - 상품 상세 보기",
                    "["+ folderName +"] GA - Event - 장바구니",
                    "["+ folderName +"] GA - Event - 주문서 작성",
                    "["+ folderName +"] GA - Event - 주문 완료"
            };
             */
            String[][] eventTrigger = {
                    {"이벤트 - 상품 상세 보기", "detail"},
                    {"이벤트 - 장바구니", "viewCart"},
                    {"이벤트 - 주문서 작성", "checkout"},
                    {"이벤트 - 주문 완료", "purchase"}
            };
            String[] uaTag = {
                    "GA - Event - 상품 상세 보기",
                    "GA - Event - 장바구니",
                    "GA - Event - 주문서 작성",
                    "GA - Event - 주문 완료"
            };

            String[][] uaTagParam = {
                    {"template", "trackType", "TRACK_EVENT"},
                    {"template", "trackingId", uaPropertyId},
                    {"template", "eventCategory", "전자상거래"},
                    {"template", "eventAction", null},
                    {"template", "gaSettings", "{{GA TRACKING ID}}"},
                    {"boolean", "useEcommerceDataLayer", "true"},
                    {"boolean", "enableEcommerce", "true"}
            };

            String[] uaTagEventAction = {"상품 상세 보기", "장바구니", "주문서 작성", "주문 완료"};

            List<Parameter> params = new ArrayList<>();
            for (String[] p : uaTagParam) params.add(new Parameter().setType(p[0]).setKey(p[1]).setValue(p[2]));

            for (int i = 0; i < uaTag.length; i++) {
                params.set(3, params.get(3).setValue(uaTagEventAction[i]));

                service.createTag(
                        uaTag[i], "ua",
                        service.createTrigger(eventTrigger[i][0], "customEvent", "equals", "{{_event}}", eventTrigger[i][1], null).getTriggerId(),
                        params
                );
            }

            //System.out.println("wait");
            //TimeUnit.MINUTES.sleep(1);
            //System.out.println("restart");

            // create DOM trigger & tag
            /*
            String[][] domTriggerParam = {
                    {"["+ folderName +"] DOM Ready - 상품 상세 보기", "/product/detail.html"},
                    {"["+ folderName +"] DOM Ready - 장바구니 페이지", "/order/basket.html"},
                    {"["+ folderName +"] DOM Ready - 주문서 작성", "/order/orderform.html"},
                    {"["+ folderName +"] DOM Ready - 주문 완료", "/order/order_result.html"}
            };
            String[][] domTagParam = {
                    {"["+ folderName +"] GA - 전자상거래 - 상품 상세 보기", "<script>dataLayer.push({'event': 'detail','ecommerce': {'detail': {'actionField': {'step': 1},'products': {{[" + folderName + "] 상품 상세 보기 - 상품 배열}}}}});</script>"},
                    {"["+ folderName +"] GA - 전자상거래 - 장바구니", "<script>dataLayer.push({ 'event': 'viewCart','ecommerce': {'currencyCode': 'KRW','add': {'actionField': {'step': 2},'products': {{[" + folderName + "] 장바구니 보기 - 상품 배열}} }} });</script>"},
                    {"["+ folderName +"] GA - 전자상거래 - 주문서 작성", "<script> dataLayer.push({ 'event': 'checkout', 'ecommerce': { 'checkout': { 'actionField': {'step': 3},'products': {{[" + folderName + "] 주문서 작성 - 상품 배열}}}}});</script>"},
                    {"["+ folderName +"] GA - 전자상거래 - 주문 완료", "<script>dataLayer.push({'event': 'purchase','ecommerce': {'purchase': {'actionField': {'id': {{[" + folderName + "] 주문 완료 - 주문 번호}},'revenue': {{[" + folderName + "] 주문 완료 - 결제 금액}},'shipping': {{[" + folderName + "] 주문 완료 - 배송비}}, },'products': {{[" + folderName + "] 주문 완료 - 상품 배열}} }} });</script>"}
            };
             */
            String[][] domTriggerParam = {
                    {"DOM Ready - 상품 상세 보기", "/product/detail.html"},
                    {"DOM Ready - 장바구니 페이지", "/order/basket.html"},
                    {"DOM Ready - 주문서 작성", "/order/orderform.html"},
                    {"DOM Ready - 주문 완료", "/order/order_result.html"}
            };
            String[][] domTagParam = {
                    {"GA - 전자상거래 - 상품 상세 보기", "<script>dataLayer.push({'event': 'detail','ecommerce': {'detail': {'actionField': {'step': 1},'products': {{상품 상세 보기 - 상품 배열}}}}});</script>"},
                    {"GA - 전자상거래 - 장바구니", "<script>dataLayer.push({ 'event': 'viewCart','ecommerce': {'currencyCode': 'KRW','add': {'actionField': {'step': 2},'products': {{장바구니 보기 - 상품 배열}} }} });</script>"},
                    {"GA - 전자상거래 - 주문서 작성", "<script> dataLayer.push({ 'event': 'checkout', 'ecommerce': { 'checkout': { 'actionField': {'step': 3},'products': {{주문서 작성 - 상품 배열}}}}});</script>"},
                    {"GA - 전자상거래 - 주문 완료", "<script>dataLayer.push({'event': 'purchase','ecommerce': {'purchase': {'actionField': {'id': {{주문 완료 - 주문 번호}},'revenue': {{주문 완료 - 결제 금액}},'shipping': {{주문 완료 - 배송비}}, },'products': {{주문 완료 - 상품 배열}} }} });</script>"}
            };

            for (int i = 0; i < domTagParam.length; i++)
                service.createTag(
                        domTagParam[i][0], "html",
                        service.createTrigger(domTriggerParam[i][0], "domReady", "equals", "{{Page Path}}", domTriggerParam[i][1],
                                Collections.singletonList(new Parameter().setType("boolean").setKey("dom").setValue("false"))).getTriggerId(),
                        Collections.singletonList(new Parameter().setType("template").setKey("html").setValue(domTagParam[i][1]))
                );

            // All Page View tag
            params.set(0, params.get(0).setValue("TRACK_PAGEVIEW"));

            //service.createTag("[" + folderName + "] All Page View", "ua", "2147479553", params);
            service.createTag("All Page View", "ua", "2147479553", params);

        } catch (IOException e) {
            log.error(e.getMessage());
            return false;
        }

        return true;
    }

    public void setIframeGTM(GTM service, GtmCSSSelectorModel selector, String propertyId) throws IOException {
        log.info("[create iframe GTM]");

        log.info(selector.getName());

        Folder folder = service.createFolder(selector.getName());
        String folderName = selector.getName();
        service.setFolderId(folder.getFolderId());

        service.createVariable("[" + folderName + "] GA TRACKING ID", "gas", "trackingId", propertyId);

        String varScript = "function() { " +
                "var productInfo = [{" +
                "   'id' : " + selector.getPNo() + "," +
                "   'name' : " + selector.getPName() + "," +
                "   'price' : " + selector.getPPrice() + "," +
                "   'quantity' : " + selector.getPQuantity() +
                "}]; " +
                "console.log(productInfo); " +
                "return productInfo;}";
        service.createVariable("[" + folderName + "] 상품 배열", "jsm", "javascript", varScript);

        // create ua trigger & tag
        Trigger viewCartTrigger = service.searchTriggerByName("Event - 장바구니");
        if (viewCartTrigger == null)
            viewCartTrigger = service.createTrigger("Event - 장바구니", "customEvent", "equals", "{{_event}}", "viewCart", null);

        Trigger purchaseTrigger = service.searchTriggerByName("Event - 구매하기");
        if (purchaseTrigger == null)
            purchaseTrigger = service.createTrigger("Event - 구매하기", "customEvent", "equals", "{{_event}}", "purchase", null);

        String[][] uaTagParam = {
                {"template", "trackType", "TRACK_EVENT"},
                {"template", "trackingId", propertyId},
                {"template", "eventCategory", "전자상거래"},
                {"template", "eventAction", "장바구니"},
                {"template", "gaSettings", "{{[" + folderName + "] GA TRACKING ID}}"},
                {"boolean", "useEcommerceDataLayer", "true"},
                {"boolean", "enableEcommerce", "true"}
        };

        List<Parameter> params = new ArrayList<>();
        for (String[] p : uaTagParam) params.add(new Parameter().setType(p[0]).setKey(p[1]).setValue(p[2]));

        service.createTag("[" + folderName + "] GA - Event - 장바구니", "ua", viewCartTrigger.getTriggerId(), params);
        params.set(3, params.get(3).setValue("구매하기"));
        service.createTag("[" + folderName + "] GA - Event - 구매하기", "ua", purchaseTrigger.getTriggerId(), params);

        // create html tag
        String viewCartHTML =
                "<script>" +
                        selector.getCartBtn() + ".on('click', function() {" +
                        "dataLayer.push({ ecommerce: null });" +
                        "dataLayer.push({" +
                        "'event': 'viewCart'," +
                        "'ecommerce': {" +
                        "'currencyCode': 'KRW'," +
                        "'add': {" +
                        "'products': {{[" + folderName + "] 상품 배열}}" +
                        "}" +
                        "}" +
                        "});" +
                        "});" +
                        "</script>";

        String purchaseHTML =
                "<script>" +
                        selector.getPurchaseBtn() + ".on('click', function(e) {" +
                        "var data = {{[" + folderName + "] 상품 배열}};" +
                        "var randomId = Math.ceil(Math.random() * (9999999 - 1) + 1);" +
                        "var price = data[0].price * 1;" +
                        "dataLayer.push({ecommerce:null});" +
                        "dataLayer.push({" +
                        "'event':'purchase'," +
                        "'ecommerce':{" +
                        "'purchase':{" +
                        "'actionField':{" +
                        "'id':randomId+''," +
                        "'revenue':price*data[0].quantity\n" +
                        "}," +
                        "'products': data" +
                        "}" +
                        "}" +
                        "});" +
                        "});" +
                        "</script>";

        service.createTag(
                "[" + folderName + "] Event - 장바구니 클릭", "html", "2147479553",
                Collections.singletonList(new Parameter().setType("template").setKey("html").setValue(viewCartHTML))
        );
        service.createTag(
                "[" + folderName + "] Event - 구매하기 클릭", "html", "2147479553",
                Collections.singletonList(new Parameter().setType("template").setKey("html").setValue(purchaseHTML))
        );
    }

    public Boolean setAllIframeGTM(ProductModel product, String containerName) {
        String propertyId = product.getPropertyId();
        GTM service;

        try {
            service = new GTM();
        } catch (GeneralSecurityException | IOException e) {
            return false;
        }

        try {
            Container container = service.createContainer(accountId, containerName);
            service.setParentPath(accountId, container.getContainerId());

            List<GtmCSSSelectorModel> selectors = gtmCssRepo.findAll();
            for (GtmCSSSelectorModel i : selectors) {
                setIframeGTM(service, i, propertyId);

                //TimeUnit.MINUTES.sleep(1);
            }

        } catch (IOException e) {
            log.error(e.getMessage());
            return false;
        }

        try {
            service.versionPublish();
        } catch (IOException e) {
            log.error(e.getMessage());
            return false;
        }

        return true;
    }

    public String setAll(ProductModel product, String containerName) {
        //String accountId = "6004589006";
        String propertyId = product.getPropertyId();
        GTM service;

        try {
            service = new GTM();
        } catch (GeneralSecurityException | IOException e) {
            log.error(e.getMessage());
            return null;
        }

        try {
            log.info(containerName);

            Container container = service.createContainer(accountId, containerName);
            service.setParentPath(accountId, container.getContainerId());
            log.info(service.getParentPath());

            List<GtmCSSSelectorModel> selectors = gtmCssRepo.findAll();
            for (GtmCSSSelectorModel i : selectors) {
                setIframeGTM(service, i, propertyId);
                //System.out.println("wait");
                //TimeUnit.MINUTES.sleep(1);
                //System.out.println("restart");
            }

            setCafe24GTM(propertyId, container.getContainerId(), "cafe24");
            log.info(service.versionPublish().getContainerVersionId() + " version published.");

            return container.getPublicId();

        } catch (IOException | InterruptedException e) {
            log.error(e.getMessage());
            return null;
        }
    }
}
