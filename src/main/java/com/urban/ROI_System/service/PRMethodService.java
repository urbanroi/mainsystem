package com.urban.ROI_System.service;

import com.urban.ROI_System.model.CrawlSiteListModel;
import com.urban.ROI_System.model.PRMethodModel;
import com.urban.ROI_System.repository.CrawlSiteListRepository;
import com.urban.ROI_System.repository.PRMethodRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class PRMethodService {
    @Autowired private PRMethodRepository methodRepo;
    @Autowired private CrawlSiteListRepository siteRepo;

    public List<PRMethodModel> findByCrawlSiteListNo(Long siteNo) {
        CrawlSiteListModel siteModel = siteRepo.findByNo(siteNo);
        if (siteModel == null) return null;

        List<PRMethodModel> list = methodRepo.findByCrawlSiteListModel(siteModel);
        if (list == null) return null;

        return list;
    }
}
