package com.urban.ROI_System.service;

import com.urban.ROI_System.model.BigCategoryModel;
import com.urban.ROI_System.repository.BigCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class BigCategoryService {

    @Autowired private BigCategoryRepository bigCategoryRepository;

    @Transactional
    public List<BigCategoryModel> getAll() {
        return bigCategoryRepository.findAll();
    }

    @Transactional
    public BigCategoryModel getOne(int no) {
        return bigCategoryRepository.findByNo(no);
    }
}
