package com.urban.ROI_System.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.urban.ROI_System.model.UserModel;
import com.urban.ROI_System.repository.UserRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserService {
	
	@Autowired private UserRepository ur;
	
	private BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
	
	public List<UserModel> findAll() { return new ArrayList<>(ur.findAll()); }

	public UserModel findByNo(Long no) { return ur.findByNo(no); }
	
	// login
	public boolean findByIdAndPw(UserModel user) {

		//log.info(encoder.encode("1").toString());
		// id 찾기 실패
		UserModel matchUser = ur.findById(user.getId());
		if (matchUser == null) return false;
		
		// pw 찾기 실패
		if (!encoder.matches(user.getPw(), matchUser.getPw())) {
			log.info(matchUser.toString());
			return false;
		}
		
		return true;
	}
	
	// id check
	public UserModel findById(String id) {
		return ur.findById(id);
	}

	// email check
	public UserModel findByEmail(String email) {
		return ur.findByEmail(email);
	}
	
	public void save(UserModel user) {
		user.setPw(encoder.encode(user.getPw()));
		if (user != null) ur.save(user);
	}

}