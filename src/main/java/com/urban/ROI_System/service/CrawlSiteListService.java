package com.urban.ROI_System.service;

import com.urban.ROI_System.model.CrawlSiteListModel;
import com.urban.ROI_System.repository.CrawlSiteListRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CrawlSiteListService {

    @Autowired private CrawlSiteListRepository siteRepo;

    public List<CrawlSiteListModel> findAll() {
        return siteRepo.findAll();
    }
}
