package com.urban.ROI_System.service;

import com.urban.ROI_System.model.ProjectLogDTO;
import com.urban.ROI_System.model.ProjectLogModel;
import com.urban.ROI_System.repository.ProjectLogRepository;
import com.urban.ROI_System.vo.ProjectLogVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Slf4j
@Service
public class ProjectLogService {

    @Autowired
    ProjectLogRepository projectLogRepository;

    public List<ProjectLogVO> getProjectLogList(Long no) {
        List<ProjectLogVO> list = new ArrayList<>();
        for (ProjectLogDTO dto : projectLogRepository.getProject(no)) {
            ProjectLogVO vo = new ProjectLogVO();
            vo.setCreateDate(dto.getCreateDate());
            vo.setROI(dto.getROI());
            vo.setDirect(dto.getDirect());
            vo.setIndirect(dto.getIndirect());
            list.add(vo);
        }
        return  list;
    }

    public void save(ProjectLogModel projectLog) {
        if (projectLog != null) projectLogRepository.save(projectLog);
    }
}