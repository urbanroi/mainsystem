package com.urban.ROI_System.service;

import com.google.api.services.tagmanager.model.Container;
import com.urban.ROI_System.google_api.google_tag_manager.GTM;
import com.urban.ROI_System.google_api.reporting.Reporting;
import com.urban.ROI_System.model.GtmTagModel;
import com.urban.ROI_System.repository.GtmTagRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;

@Service
@Slf4j
public class DataUpdateService {

    @Autowired private ProductService productService;
    @Autowired private ProductLogService productLogService;
    @Autowired private ProjectService projectService;

    @Autowired private GtmTagRepository tagRepo;

    /**
     * Product, ProductLog 데이터 업데이트
     *     1. Reporting API 에서 하루 총 매출 조회
     *     2. Project 에서 합산하여 총 투자비용 조회
     *     3. ROI 계산 (총 투자비용 / 총 매출)
     *     4. ProductLog 저장
     *     5. Product 업데이트
     * @param product : ProductModel 객체
     */
//    public void updateData(ProductModel product) {
//        try {
//            long totalSales = getSingleDayData(product.getViewId());
//            long totalProperty;
//            try { totalProperty = projectService.getTotalPropertyByProductNo(product.getNo()); }
//            catch (NullPointerException e) { totalProperty = 0L; }
//
//            double totalROI = getProductROI(product.getTotalProperty(), product.getTotalSales());
//
//            log.info(product.getProductName());
//
//            // 24시간 내에 기록된 로그가 있는 경우 해당 로그를 업데이트
//            try {
//                ProductLogModel lastLog = productLogService.getLastLog(product);
//
//                if (lastLog.getCreateDate().toString().split("T")[0].equals(LocalDateTime.now().toString().split("T")[0])) {
//                    lastLog.setTotalSales(totalSales);
//                    lastLog.setTotalProperty(totalProperty);
//                    lastLog.setTotalRoi(totalROI);
//
//                    /*
//                    log.info("[LastLog]");
//                    log.info("TotalProperty : " + lastLog.getTotalProperty());
//                    log.info("TotalSales : " + lastLog.getTotalSales());
//                    log.info("TotalROI : " + lastLog.getTotalRoi());
//                     */
//                    productLogService.save(lastLog);
//                } else {
//                    ProductLogModel productLog =
//                            ProductLogModel.builder()
//                                    .productModel(product)
//                                    .totalSales(totalSales)
//                                    .totalProperty(totalProperty)
//                                    .totalRoi(totalROI)
//                                    .build();
//                    productLogService.save(productLog);
//                }
//            // 로그 없는경우
//            } catch (NullPointerException e) { log.info("a new log has been created."); }
//
//            product.setTotalSales(totalSales);
//            product.setTotalProperty(totalProperty);
//            product.setTotalRoi(totalROI);
//            productService.save(product);
//
//        } catch (IOException | GeneralSecurityException e) {
//            e.printStackTrace();
//        }
//    }

    /**
     * Reporting API 를 이용해 하루 분의 매출 데이터 조회
     * @param viewId : Product 의 애널리틱스 보기 고유번호
     * @return ReportData
     */
    public long getSingleDayData(String viewId) throws IOException, GeneralSecurityException, NullPointerException {
        Reporting service = new Reporting();

        service.initialize(viewId);
        String result = service.getSingleDayRevenueData();
//        result.forEach((k, v) -> System.out.println(k + " : " + v));

        // ReportData = { totals : { values : [] } }
        return result != null ? Long.parseLong(result) : 0L;
    }

    /**
     * ROI 계산
     * @param property : 총 투입비용
     * @param totalSales : 총 매출
     * @return
     */
    public double getProductROI (Long property, Long totalSales) {
        if (property == 0 || totalSales == 0) return 0;
        return (double) Math.round((double) totalSales / (double) property * 10000.0) / 100.0;
    }

    @Value("${analytics.gtmAccount}")
    private String accountId;

    public void deleteData() throws GeneralSecurityException, IOException {
        GTM gtm = new GTM();
        gtm.initialize();

        //String accountId = "6004589006";

        List<Container> containers = gtm.getContainers(accountId);
        List<GtmTagModel> gtms = tagRepo.findAll();

        if (gtms.isEmpty()) { gtm.deleteAllContainer(accountId); return; }
        //if (containers.size() == 0 && gtms.size() == 0) return;

        for (GtmTagModel i : gtms) {
            for (Container j : containers) {
                if (j.getPublicId().equals(i.getGtmTag())) { containers.remove(j); break; }
            }
        }

        if (containers.size() > 0) {
            for (Container i : containers) gtm.deleteContainer(accountId, i.getContainerId());
        }
    }
}