package com.urban.ROI_System.service;

import com.urban.ROI_System.model.ProductModel;
import com.urban.ROI_System.model.UserModel;
import com.urban.ROI_System.repository.ProductLogRepository;
import com.urban.ROI_System.repository.ProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@Slf4j
public class ProductService {
    @Autowired private ProductRepository prodRepo;
    @Autowired private ProductLogRepository logRepo;

    public List<ProductModel> findAllByDate(LocalDateTime startDate, LocalDateTime endDate, UserModel userModel) {
        List<ProductModel> prods = prodRepo.findAllByCreateDateBetweenAndUserModelOrderByTotalSalesDesc(startDate, endDate, userModel);
        prods.forEach(i -> System.out.println(i.getProductName()));
        return prods;
    }

    public List<ProductModel> findAllByLogCreatedDate(LocalDateTime startDate, LocalDateTime endDate, UserModel userModel) {
  //      return prodRepo.getProductByUserModelAndLogCreateDate(startDate, endDate, userModel.getNo());

        List<ProductModel> prods = prodRepo.findByUserModel(userModel);

        prods.forEach(i -> {
            i.setProductLogModel(logRepo.findAllByCreateDateBetweenAndProductModel(startDate, endDate, i));
        });

        return prods;
    }

    public List<ProductModel> findByUserNo(UserModel model) {
        return prodRepo.findByUserModel(model);
    }

    public ProductModel save(String productName, String accountId, UserModel userModel) {
        ProductModel prod = ProductModel.builder().productName(productName).accountId(accountId).userModel(userModel).build();

        log.info(prod.toString());

        return prodRepo.save(prod);
    }

    public ProductModel save(ProductModel prod) {
        return prodRepo.save(prod);
    }

    /*
    public ProductModel save(String productName, UserModel userModel) {
        ProductModel prod = ProductModel.builder().productName(productName).userModel(userModel).build();
        return prodRepo.save(prod);
    }
     */

    public List<ProductModel> findByPage(int page, int size, UserModel userModel) {
        PageRequest pageRequest = PageRequest.of(page - 1, size, Sort.by(Sort.Direction.DESC, "no"));
        List<ProductModel> list = prodRepo.findByUserModel(userModel, pageRequest).getContent();
        if (list.size() == 0) return null;

        return list;
    }

    public List<ProductModel> findByProductName(int page, int size, UserModel userModel, String productName) {
        PageRequest pageRequest = PageRequest.of(page - 1, size, Sort.by(Sort.Direction.DESC, "no"));
        List<ProductModel> list = prodRepo.findByProductNameContainingIgnoreCaseAndUserModel(productName, userModel, pageRequest).getContent();
        if (list.size() == 0) return null;

        return list;
    }

    public Long count() {
        return prodRepo.count();
    }
    public Long countByUserModel(UserModel userModel) {
        return prodRepo.countByUserModel(userModel);
    }

    public Long countByUserModelAndProductName(UserModel userModel, String productName) {
        return prodRepo.countByProductNameContainingIgnoreCaseAndUserModel(productName, userModel);
    }

    public ProductModel findByNo(Long no) {
        return prodRepo.findByNo(no);
    }
    public ProductModel findByAccountId(String accountId) { return prodRepo.findByAccountId(accountId); }

    public ProductModel updatePropertyId(String accountId, String propertyId, String viewId) {
        log.info("propertyId : " + propertyId);

        ProductModel prod = prodRepo.findByAccountId(accountId);
        if (prod == null) return null;

        prod.setPropertyId(propertyId);
        prod.setViewId(viewId);
        prodRepo.save(prod);

        return prod;
    }



}