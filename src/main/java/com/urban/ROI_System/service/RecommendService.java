package com.urban.ROI_System.service;

import com.urban.ROI_System.model.RecommendModel;
import com.urban.ROI_System.repository.RecommendRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class RecommendService {

    @Autowired
    private RecommendRepository recommendRepository;

    private final int RESULT_SIZE = 3;

    public List<RecommendModel> getSortedRecommendData(Integer smallCategoryNo, Integer bigCategoryNo, Integer age, Character gender) {
        //gender ALL = A
        //age ALL = 99

        RecommendModel parameter = RecommendModel.builder().smallCategoryNo(smallCategoryNo).bigCategoryNo(bigCategoryNo).age(age).gender(gender).build();

        List<RecommendModel> resultList = new ArrayList<>();
        List<RecommendModel> FindAll = recommendRepository.findAll();

        for (RecommendModel a:FindAll) {
            if(a.equalsScore(parameter) >= 50) {
                resultList.add(a);
            } //if
        } // for

        if(resultList.size() >= 2){
            resultList.sort((o1, o2) -> (o1.getScore() + (o1.getRoi()/10.0)) < (o2.getScore() + (o2.getRoi()/10.0))? 1:-1);
        }
        ArrayList<RecommendModel> returnList = new ArrayList<RecommendModel>();
        for (int i = 0; i < RESULT_SIZE; i++) {
            try {
                if(resultList.get(i) != null){
                    returnList.add(resultList.get(i));
                }
            } catch (ArrayIndexOutOfBoundsException e){ log.info("recommend index > RESULT_SIZE"); }
        }

        return returnList;
    }
}
