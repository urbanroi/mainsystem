package com.urban.ROI_System.exception;

public class UTMUnknownTypeException extends Exception {
    UTMUnknownTypeException() {
        super("UTM에 타입이 명시되어 있지 않습니다");
    }
}
