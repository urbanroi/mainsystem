package com.urban.ROI_System.exception;

public class RegularException extends Exception {
    public RegularException(String caption) {
        super(caption);
    } // RegularException();
} // class