package com.urban.ROI_System.exception.validity;

import com.urban.ROI_System.exception.RegularException;
import com.urban.ROI_System.model.ProductModel;

public class ProductRegularChecker implements RegularChecker {

    @Override
    public void checkAll(Object obj) throws RegularException {
        ProductModel product = (ProductModel)obj;
        checkName(product.getProductName());
    } //checkAll();

    public void checkName(String name) throws RegularException {
        if(!name.matches("^[0-9A-z가-힣]{1,20}$")) {
            throw new RegularException("제품명을 확인해 주세요");
        } //if
    } //checkName();
} //ProductRegularChecker();