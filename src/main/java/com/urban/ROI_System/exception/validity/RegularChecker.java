package com.urban.ROI_System.exception.validity;

public interface RegularChecker {
    public void checkAll(Object obj) throws Exception;
}