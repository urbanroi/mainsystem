package com.urban.ROI_System.exception.validity;

import com.urban.ROI_System.exception.RegularException;
import com.urban.ROI_System.model.UserModel;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UserRegularChecker implements RegularChecker {

	@Override
	public void checkAll(Object obj) throws RegularException {
		UserModel a = (UserModel)obj;

		log.info(a.toString());

		idChecker(a.getId());
		pwChecker(a.getPw());
		mailChecker(a.getEmail());
		nameChecker(a.getName());
	} //Constructor();
	
	public void idChecker(String a) throws RegularException {
		if(!a.matches("^[A-z][A-z0-9]{7,15}$")) {
			throw new RegularException("ID를 확인해 주세요");
		}
	} //idChecker();

	public void pwChecker(String a) throws RegularException {
		if(!a.matches("^[A-z][A-z0-9]{7,15}$")) {
			throw new RegularException("비밀번호를 확인해 주세요");
		}
	} //pwChecker();

	public void mailChecker(String a) throws RegularException {
		if(!a.matches("^[0-9a-zA-Z]([-_\\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\\.]?[0-9a-zA-Z])*\\.[a-zA-Z]{2,3}$")) {
			throw new RegularException("이메일을 확인해 주세요");
		}
	} //mailChecker();

	public void nameChecker(String a) throws RegularException {
		if(!a.matches("^[가-힣]{2,4}|[a-zA-Z]{2,10}\\s[a-zA-Z]{2,10}$")) {
			throw new RegularException("이름을 확인해 주세요");
		}
	} //nameChecker();
} //class