package com.urban.ROI_System.exception.validity;

import com.urban.ROI_System.exception.RegularException;
import com.urban.ROI_System.model.ProjectModel;
import com.urban.ROI_System.repository.CrawlSiteListRepository;
import com.urban.ROI_System.repository.PRMethodRepository;
import com.urban.ROI_System.repository.ProductRepository;
import com.urban.ROI_System.util.BeanUtil;
import com.urban.ROI_System.service.LinkValidationService;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;

@Slf4j
public class ProjectRegularChecker implements RegularChecker {

    @Override
    public void checkAll(Object obj) throws Exception {
        ProjectModel projectModel = (ProjectModel)obj;

        nameCheck(projectModel.getProjectName());
        dateChecker(projectModel.getProjectStartDate(), projectModel.getProjectEndDate());
//        linkChecker(projectModel.getLink());
        propertyChecker(projectModel.getProperty());
        siteNoChecker(projectModel.getCrawlSiteListModel().getNo());
        productNoChecker(projectModel.getProductModel().getNo());
        //prMethodChecker(projectModel.getPrMethodModel().getNo());
    } //checkAll();

    public void nameCheck(String name) throws RegularException {
        if (name.matches("/[A-z가-힣0-9]{1,20}$/")) {
            throw new RegularException("프로젝트명을 확인해 주세요");
        }
    } //nameCheck();

    public void dateChecker(LocalDateTime beforeDate, LocalDateTime afterDate) throws RegularException {
        if (!beforeDate.isBefore(afterDate)) {
            throw new RegularException("날짜를 확인해 주세요");
        }
    } //dateChecker();

    public void linkChecker(String link) throws RegularException {

        if (!link.matches("https?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_\\+.~#?&//=]*)")) {
            throw new RegularException("링크 주소를 확인해 주세요");
        } else {
            int linkResult = BeanUtil.getBean(LinkValidationService.class).send(link);
            if (linkResult == 400) throw new RegularException("필드의 형식이 잘못 되었습니다");
            else if (linkResult == 404) throw new RegularException("찾을 수 없는 링크입니다");
        }

    } // linkChecker();

    public void propertyChecker(Long property) throws RegularException {
        if (property >= 1000000000000L) {
        	throw new RegularException("투입 금액을 확인해 주세요");
        } //if
    } //sales();

    public void siteNoChecker(Long sNo) throws RegularException {
        if (sNo != 999) {
            boolean result = BeanUtil.getBean(CrawlSiteListRepository.class).existsByNo(sNo); // 존재 여부 확인
            if(!result) {
                throw new RegularException("사이트 번호가 잘못 되었습니다");
            } //if
        }
    } //siteNoChecker();

    public void prMethodChecker(Long prNo) throws RegularException {
        boolean result = BeanUtil.getBean(PRMethodRepository.class).existsByNo(prNo); // 존재 여부 확인
        if(!result) {
            throw new RegularException("지원 하지 않는 홍보 매체 입니다");
        } //if
    } //prMethodChecker();

    public void productNoChecker(Long pdNo) throws RegularException {
        boolean result = BeanUtil.getBean(ProductRepository.class).existsByNo(pdNo); // 존재 여부 확인
        if(!result) {
            throw new RegularException("없는 제품 번호 입니다");
        } //if
    } //productNoChecker();

} //projectRegularChecker