package com.urban.ROI_System.vo;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.Date;

@Data
public class Token {

    @JsonAlias("access_token")
    private String accessToken;

    @JsonAlias("expires_in")
    private Integer expiresIn;

    @JsonIgnore
    private Date expiresDate;

    @JsonAlias("refresh_token")
    private String refreshToken;
    private String scope;


    @JsonAlias("token_type")
    private String tokenType;

    @JsonAlias("id_token")
    private String idToken;

    public void setExpiresIn(Integer expiresIn) {
        this.expiresIn = expiresIn;
        setExpiresDate(new Date(new Date().getTime() + Long.parseLong(expiresIn * 1000 +"")));
    }
} //class