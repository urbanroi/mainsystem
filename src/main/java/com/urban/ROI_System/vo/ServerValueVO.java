package com.urban.ROI_System.vo;

import com.urban.ROI_System.util.ServerValue;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * 서버 변수 저장 VO
 * 사용법 ex) {@link ServerValue}.get().scope
 */

@Component
@Getter
public class ServerValueVO {
    @Value("${analytics.serviceAccount}")
    private String serviceAccount;
    @Value("${oAuth.redirectURI}")
    private String redirectURI;
    @Value("${oAuth.clientID}")
    private String clientID;
    @Value("${oAuth.clientPW}")
    private String clientPW;
    @Value("${oAuth.scope}")
    private String scope;
    @Value("${crawlServer.url}")
    private String crawlServerUrl;
    @Value("${linkTestServer.url}")
    private String linkTestServerUrl;
    @Value("${onlineURL}")
    private String onlineURL;
} // Class



