package com.urban.ROI_System.vo;

import lombok.Data;

@Data
public class AnalyticsAccountVO {
    private String name;
    private String createTime;
    private String updateTime;
    private String displayName;
    private String regionCode;
}