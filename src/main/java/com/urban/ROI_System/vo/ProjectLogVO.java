package com.urban.ROI_System.vo;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProjectLogVO {
    private Long direct;
    private Long indirect;
    private Double ROI;
    private LocalDateTime createDate;

}
