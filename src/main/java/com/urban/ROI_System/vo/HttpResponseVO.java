package com.urban.ROI_System.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class HttpResponseVO {
    private String body;
    private Integer status;
}
