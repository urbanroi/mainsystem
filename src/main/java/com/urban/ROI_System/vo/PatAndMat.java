package com.urban.ROI_System.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PatAndMat {
    private String method; // 소스
    private String medium; // 매체
}
