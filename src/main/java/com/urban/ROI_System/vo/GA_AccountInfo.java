package com.urban.ROI_System.vo;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;

@Data
public class GA_AccountInfo {
    private String id;
    private String email;
    @JsonAlias("verified_email")
    private String verifiedEmail;
    private String picture;
} //class
