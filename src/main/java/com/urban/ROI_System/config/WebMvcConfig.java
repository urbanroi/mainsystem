package com.urban.ROI_System.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.urban.ROI_System.interceptor.F_LoginInterceptor;
import com.urban.ROI_System.interceptor.RestAPILogger;
import com.urban.ROI_System.interceptor.T_LoginInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
//        registry.addInterceptor(new PublicInterceptor())
//                .addPathPatterns("/**");

        registry.addInterceptor(new T_LoginInterceptor())
                .addPathPatterns("/user/**")
                .excludePathPatterns("/user/oauth/ga-create-success");

        registry.addInterceptor(new F_LoginInterceptor())
                .addPathPatterns("/login")
                .addPathPatterns("/loginAction")
                .addPathPatterns("/join")
                .excludePathPatterns("/user/oauth/ga-create-success");

        registry.addInterceptor(new RestAPILogger(new ObjectMapper()))
                .addPathPatterns("/rest/**");


    } //addInterceptor();


} //class
