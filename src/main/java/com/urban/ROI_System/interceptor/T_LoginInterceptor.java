package com.urban.ROI_System.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.AsyncHandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
@Slf4j
public class T_LoginInterceptor implements AsyncHandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        if(request.getSession().getAttribute("loginInfo") == null) {
            log.info("Can't Access With Anonymous: " + request.getRequestURI());
            request.getSession().removeAttribute("beforeUrl");
            if(request.getMethod().equals("GET")) {
                request.getSession().setAttribute("beforeUrl",request.getRequestURL()+"?"+request.getQueryString());
            }


            request.getSession().setAttribute("msg","로그인을 해주세요");
            request.getSession().setAttribute("red","/login");
            response.sendRedirect("/error/send-alert");
        }
        return true;
    } //preHandle();

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    } //postHandle();

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    } //afterCompletion();
} //class