package com.urban.ROI_System.interceptor;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.AsyncHandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Component
@Slf4j
@RequiredArgsConstructor
public class RestAPILogger implements AsyncHandlerInterceptor {

    private final ObjectMapper objectMapper;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {

        //log.debug(request.getQueryString());

        return true;
//        StringBuilder sb = new StringBuilder();
//        for (String a : response.getHeaderNames()) {
//            sb.append(response.getHeader(a));
//        }
//        log.debug(sb.toString());
//        final ContentCachingRequestWrapper cachingRequest = (ContentCachingRequestWrapper) request;
//        final ContentCachingResponseWrapper cachingResponse = (ContentCachingResponseWrapper) response;
//
//        if (cachingRequest.getContentType() != null && cachingRequest.getContentType().contains("application/json")) {
//            if (cachingRequest.getContentAsByteArray() != null && cachingRequest.getContentAsByteArray().length != 0) {
//                log.info("Request Body : {}", objectMapper.readTree(cachingRequest.getContentAsByteArray()));
//            }
//        }
//        if (cachingResponse.getContentType() != null && cachingResponse.getContentType().contains("application/json")) {
//            if (cachingResponse.getContentAsByteArray() != null && cachingResponse.getContentAsByteArray().length != 0) {
//                log.info("Response Body : {}", objectMapper.readTree(cachingResponse.getContentAsByteArray()));
//            }
//        }
//        return false;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        if (response.getStatus() != 200) {
            log.info(request.getRequestURI());
            log.error("응답 코드"+response.getStatus());
        } //if
    } //afterCompletion();
} //class