package com.urban.ROI_System.interceptor;

import com.urban.ROI_System.model.UserModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.AsyncHandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
public class PublicInterceptor implements AsyncHandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //log.info("queryString : " + request.getQueryString());

        if (!request.getRequestURI().contains("/template/") && !request.getRequestURI().contains("/assets") && !request.getRequestURI().contains("/img")) {
            log.info("requestUri" + request.getRequestURI());
            log.info("----------------------------------------");
        }

        //log.info("requestUrl" + request.getRequestURL());

        return true;
    } //preHandle();

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    } //postHandle();

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    } //afterCompletion();
}
