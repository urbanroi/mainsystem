package com.urban.ROI_System.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.sun.istack.NotNull;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Table(name = "project_data")
@Entity
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
public class ProjectDataModel  extends MappedModel{

	@JsonBackReference
	@JsonDeserialize
	@NotNull
	@ManyToOne
	@JoinColumn(name = "project_no", updatable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private ProjectModel projectModel;

	@NotNull
	@Column(name = "count")
	private Long count;

	@NotNull
	@Column(name = "data_type")
	private Character dataType;

}