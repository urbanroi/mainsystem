package com.urban.ROI_System.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.annotations.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Table(name = "gtm_tags")
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@DynamicInsert
@DynamicUpdate
public class GtmTagModel {     
	@Id
    @Column(name="no")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long no;

    @Column(name = "gtm_tag", length = 256)
    private String gtmTag;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "product_no", updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private ProductModel productModel;
}