package com.urban.ROI_System.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.sun.istack.NotNull;
import lombok.*;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Table(name = "pr_method")
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@DynamicInsert
@DynamicUpdate
public class PRMethodModel {
     
    @Id
    @Column(name = "no")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long no;

    @NotNull
    @Column(name = "method", length = 20)
    private String method;

    @NotNull
    @Column(name = "p_type", length = 1)
    private char pType;

    @NotNull
    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "site_no")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private CrawlSiteListModel crawlSiteListModel;

    @Column(name = "medium", length = 16)
    private String medium;

    /*
    @NotNull
    @OneToOne
    @JoinColumn(name = "project_no")
    private ProjectModel projectModel;
     */
}
