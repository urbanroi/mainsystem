package com.urban.ROI_System.model;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.*;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "product_log")
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@DynamicInsert
@DynamicUpdate
public class ProductLogModel {
	@Id
    @Column(name="no")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
     private Long no;

    @Column(name="total_property")
    private Long totalProperty;

    @Column(name = "total_sales")
    private Long totalSales;

    @Column(name = "total_roi")
    private Double totalRoi;

    @CreationTimestamp
    private LocalDateTime createDate;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "product_no", updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private ProductModel productModel;
}