package com.urban.ROI_System.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Table(name = "small_category")
@Entity
@Data
@ToString(exclude = "bigCategoryModel")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@DynamicInsert
@DynamicUpdate
public class SmallCategoryModel {

    @Id
    @Column(name = "no")
    private Integer no;

    @Column(name = "name", length = 64)
    private String name;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "p_no")
    private BigCategoryModel bigCategoryModel;

}
