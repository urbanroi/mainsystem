package com.urban.ROI_System.model;

import com.sun.istack.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Table(name = "gtm_css_selector")
@Entity
@Data
@NoArgsConstructor
@DynamicInsert
@DynamicUpdate
public class GtmCSSSelectorModel {

    @Id
    @Column(name = "no")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long no;

    @Column(name = "name", length = 45)
    private String name;

    @Column(name = "url", length = 128)
    private String url;

    @Column(name = "cart_btn", length = 128)
    private String cartBtn;

    @Column(name = "purchase_btn", length = 128)
    private String purchaseBtn;

    @Column(name = "p_no", length = 128)
    private String pNo;

    @Column(name = "p_name", length = 128)
    private String pName;

    @Column(name = "p_price", length = 128)
    private String pPrice;

    @Column(name = "p_quantity", length = 128)
    private String pQuantity;

}
