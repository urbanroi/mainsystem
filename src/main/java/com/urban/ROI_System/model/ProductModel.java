package com.urban.ROI_System.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.google.api.services.analytics.model.Account;
import com.sun.istack.NotNull;
import lombok.*;
import org.hibernate.annotations.*;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "product")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@DynamicInsert
@DynamicUpdate
@ToString(exclude = {"userModel", "productLogModel", "projectModelList", "gtmTagModel"})
public class ProductModel {
    @Id
    @Column(name="no")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long no;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "user_no", updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private UserModel userModel;

    @Column(name="product_name", length = 128)
    private String productName;

    @Builder.Default
    @Column(name="total_property", columnDefinition = "bigint default 0")
    private Long totalProperty = 0L;

    @Builder.Default
    @Column(name="total_sales", columnDefinition = "bigint default 0")
    private Long totalSales = 0L;

    @Builder.Default
    @Column(name="total_ROI", columnDefinition = "double default 0")
    private Double totalRoi = 0.0;

    @NotNull
    @Column(name="account_id", length = 16)
    private String accountId;

    @Column(name="property_id", length = 16)
    private String propertyId;

    @Column(name="view_id", length = 16)
    private String viewId;

    @JsonFormat(pattern = "yyyy-MM-dd HH:ss")
    @CreationTimestamp
    private LocalDateTime createDate;

    //@JsonManagedReference
    @OneToMany(mappedBy = "productModel", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<ProductLogModel> productLogModel;

    @JsonManagedReference
    @OneToMany(mappedBy = "productModel", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<ProjectModel> projectModelList;

    @JsonManagedReference
    @OneToMany(mappedBy = "productModel", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<GtmTagModel> gtmTagModel;

    @Override
    public boolean equals(Object o) {
        Account t;
        if (o instanceof Account) {
            t = (Account) o;
            if(this.accountId == t.getId()){
                return true;
            }
            return false;
        } else {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            ProductModel that = (ProductModel) o;
            return Objects.equals(no, that.no) && Objects.equals(userModel, that.userModel) && Objects.equals(productName, that.productName) && Objects.equals(totalProperty, that.totalProperty) && Objects.equals(totalSales, that.totalSales) && Objects.equals(totalRoi, that.totalRoi) && Objects.equals(accountId, that.accountId) && Objects.equals(propertyId, that.propertyId) && Objects.equals(createDate, that.createDate) && Objects.equals(productLogModel, that.productLogModel) && Objects.equals(projectModelList, that.projectModelList);
        } //if~else
    }

    @Override
    public int hashCode() {
        return Objects.hash(no, userModel, productName, totalProperty, totalSales, totalRoi, accountId, propertyId, createDate, productLogModel, projectModelList);
    }

}