package com.urban.ROI_System.model;

public interface BigCategoryDTO {
    Long getNo();
    String getName();
}
