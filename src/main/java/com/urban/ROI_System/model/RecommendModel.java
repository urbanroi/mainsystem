package com.urban.ROI_System.model;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.util.Comparator;
import java.util.Objects;
import java.util.SortedSet;

@Table(name = "recommend")
@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
public class RecommendModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer no;
    private Character gender;
    private Integer bigCategoryNo;
    private Integer smallCategoryNo;
    private Double roi;
    private String site;
    private String type;
    private Integer age;
    @Transient
    private Integer score;

    public int equalsScore(RecommendModel o) {
        int score = 0;
        if (o == null) return 0;

        log.info("?"+no);

        if(o.getGender() == 'A' || gender == o.getGender())  {score += 25; };
        if(o.getAge() == 99 || age == o.getAge()) {score += 25; };
        if(o.getBigCategoryNo() == bigCategoryNo){score += 25; };
        if(o.getSmallCategoryNo() == smallCategoryNo){score += 25; };
        this.score = score;
        return score;
    }
    @Override
    public String toString() {
        return "RecommendModel{" +
                "no=" + no +
                ", age="+age+
                ", gender=" + gender +
                ", bigCategoryNo=" + bigCategoryNo +
                ", smallCategoryNo=" + smallCategoryNo +
                ", roi=" + roi +
                ", score=" + score +
                '}';
    }
}
