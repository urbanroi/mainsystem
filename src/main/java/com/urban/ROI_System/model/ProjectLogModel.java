package com.urban.ROI_System.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nullable;
import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "project_log")
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class ProjectLogModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long no;

    @NotNull
    private Long indirect;

    @NotNull
    private Long direct;

    @NotNull
    private Long total;

    @Nullable
    private Long brandValue;

    @CreationTimestamp
    private LocalDateTime createDate;

    @JsonBackReference
    @ManyToOne
    @NotNull
    @JoinColumn(name="project_no", updatable =false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private ProjectModel projectModel;
}