package com.urban.ROI_System.model;

import java.time.LocalDateTime;

public interface ProjectLogDTO {
    LocalDateTime getCreateDate();
    Long getDirect();
    Long getIndirect();
    Double getROI();
}
