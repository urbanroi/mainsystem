package com.urban.ROI_System.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.sun.istack.NotNull;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Table(name = "crawl_site_list")
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@DynamicInsert
@DynamicUpdate
public class CrawlSiteListModel {
     
    @Id
    @Column(name="no")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long no;

    @NotNull
    @Column(name="name")
    private String name;

    @NotNull
    @Column(name="link")
    private String link;
    
    @CreationTimestamp
    private LocalDateTime createDate;

    @JsonManagedReference
    @OneToMany(mappedBy = "crawlSiteListModel", fetch = FetchType.LAZY)
    private List<ProjectModel> projectModel;

    @JsonManagedReference
    @OneToMany(mappedBy = "crawlSiteListModel", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<PRMethodModel> prMethodModel;
}