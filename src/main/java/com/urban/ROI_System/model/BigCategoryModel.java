package com.urban.ROI_System.model;

import lombok.*;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.List;

@Table(name = "big_category")
@Entity
@Data
@ToString(exclude = "smallCategoryList")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@DynamicInsert
@DynamicUpdate
public class BigCategoryModel {

    @Id
    @Column(name = "no")
    private Integer no;

    @Column(name = "name", length = 64)
    private String name;

    @OneToMany(mappedBy = "bigCategoryModel", cascade = CascadeType.ALL)
    private List<SmallCategoryModel> smallCategoryList;

}
