package com.urban.ROI_System.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.sun.istack.NotNull;
import lombok.*;
import org.hibernate.annotations.*;

import javax.annotation.Nullable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

@Table(name = "project")
@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@DynamicInsert
@DynamicUpdate
public class ProjectModel {
	 
	@Id
	@Column(name = "no")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long no;

	@NotNull
	@Column(name = "last_collection_date")
	private LocalDateTime lastCollectionDate;

	@NotNull
	@Column(name = "project_name",length = 45)
	private String projectName;

	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "product_no", updatable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private ProductModel productModel;

	@NotNull
	@Column(name = "project_start_date")
	private LocalDateTime projectStartDate;

	@NotNull
	@Column(name = "project_end_date")
	private LocalDateTime projectEndDate;

	@Builder.Default
	@NotNull
	@Column(name = "sales")
	private Long sales = 0L;

	@Builder.Default
	@NotNull
	@Column(name = "estimated_contributior")
	private Long estimatedContribution = 0L;

	@Column(name = "link", length = 256)
	private String link;

	@Column(name = "utm", length = 256)
	private String utm;

	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "site_no", updatable = false)
	private CrawlSiteListModel crawlSiteListModel;

	@NotNull
	@Builder.Default
	@Column(name = "property")
	private Long property = 0L;

	@Column(name = "link_validity")
	@ColumnDefault("true")
	private boolean linkValidity;

	/*
	@OneToOne(mappedBy = "projectModel")
	@JoinColumn(name = "method_no")
	private PRMethodModel prMethodModel;
	 */

	@JsonManagedReference
	@OneToMany(mappedBy = "projectModel", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
	private List<ProjectDataModel> projectData;

	@JsonManagedReference
	@OneToMany(mappedBy = "projectModel",fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	private List<ProjectLogModel> projectLogList;

	@Nullable
	private Long brandValue;

    public ProjectModel(Long projectNo) {
    	this.no = projectNo;
    }

    @Column(name = "age")
	private Integer age;

    @Column(name = "gender", length = 1)
	private Character gender;

    @OneToOne
    @JoinColumn(name = "big_category_no")
	private BigCategoryModel bigCategoryModel;

    @OneToOne
    @JoinColumn(name = "small_category_no")
	private SmallCategoryModel smallCategoryModel;

    public String getSimpleProjectEndDate(){
		return this.projectEndDate.format( DateTimeFormatter.ofPattern("yyyy-MM-dd"));
	}

}