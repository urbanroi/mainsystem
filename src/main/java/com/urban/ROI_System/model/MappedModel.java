package com.urban.ROI_System.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@MappedSuperclass
@Setter
@Getter
public abstract class MappedModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="no")
    private Long no;

    @CreationTimestamp
    private LocalDateTime createDate;

}
