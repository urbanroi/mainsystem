package com.urban.ROI_System.google_api.reporting;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.analytics.Analytics;
import com.google.api.services.analytics.model.McfData;
import com.google.api.services.analyticsreporting.v4.AnalyticsReporting;
import com.google.api.services.analyticsreporting.v4.AnalyticsReportingScopes;
import com.google.api.services.analyticsreporting.v4.model.*;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Slf4j
public class Reporting {

    private AnalyticsReporting service;
    private Analytics mcfService;
    private String viewId;
    private static String KEY_FILE_LOCATION;
    private static final String APPLICATION_NAME = "ROI System";

    static {
        String localPath = "src/main/java/com/urban/ROI_System/config/key/urbanroi-663efb743341.json";
        String serverPath = "apiKey.json";

        File file = new File(localPath);
        KEY_FILE_LOCATION =  file.exists() ? localPath : serverPath;
    }

    public void initialize(String viewId) throws GeneralSecurityException, IOException {
        HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
        JsonFactory JSON_FACTORY = GsonFactory.getDefaultInstance();
        GoogleCredential credential = GoogleCredential
                .fromStream(new FileInputStream(KEY_FILE_LOCATION))
                .createScoped(AnalyticsReportingScopes.all());

        this.service = new AnalyticsReporting
                .Builder(httpTransport, JSON_FACTORY, credential)
                .setApplicationName(APPLICATION_NAME)
                .build();

        this.mcfService = new Analytics
                .Builder(httpTransport, JSON_FACTORY, credential)
                .setApplicationName(APPLICATION_NAME)
                .build();
        this.viewId = viewId;
    }

    /**
     * 하루 총매출 조회
     */
    public String getSingleDayRevenueData() {
        if (service == null) {
            log.error("service error");
            return null;
        }

        DateRange dateRange = new DateRange().setStartDate("1DaysAgo").setEndDate("today");
        Metric metric = new Metric().setExpression("ga:transactionRevenue").setFormattingType("INTEGER");
        ReportRequest request = new ReportRequest()
                .setViewId(viewId)
                .setDateRanges(Collections.singletonList(dateRange))
                .setMetrics(Collections.singletonList(metric));

        try {
            return service
                .reports()
                .batchGet(new GetReportsRequest().setReportRequests(Collections.singletonList(request)))
                .execute().getReports().get(0).getData()
                .getTotals().get(0).getValues().get(0);
        } catch (NullPointerException e) {
            log.warn("total revenue is zero");
            return "0";
        }
        catch (IOException e) {
            log.error(e.getMessage());
            return null;
        }
    }


    /**
     * 광고 콘텐츠가 'urban-roi' 인 광고의 직접기여 조회
     * 각 ReportRow 예시 :
     * {"dimensions":["urban-roi","projectNo"],"metrics":[{"values":["직접기여"]}]}
     * @return List<ReportRow> directList
     */
    public List<ReportRow> getDirectList() {
        if (service == null) {
            log.error("service error");
            return null;
        }

        DateRange dateRange = new DateRange().setStartDate("1DaysAgo").setEndDate("today");
        List<Dimension> dimensions = new ArrayList<>();
        List<DimensionFilter> filters = new ArrayList<>();

        dimensions.add(new Dimension().setName("ga:adContent"));
        dimensions.add(new Dimension().setName("ga:campaignCode"));

        filters.add(new DimensionFilter().setDimensionName("ga:adContent").setOperator("EXACT").setExpressions(Collections.singletonList("urban-roi")));

        DimensionFilterClause dimensionFilter = new DimensionFilterClause().setFilters(filters);
        Metric metric = new Metric().setExpression("ga:transactionRevenue").setFormattingType("INTEGER");

        ReportRequest request = new ReportRequest()
                .setViewId(viewId)
                .setDateRanges(Collections.singletonList(dateRange))
                .setDimensions(dimensions)
                .setDimensionFilterClauses(Collections.singletonList(dimensionFilter))
                .setMetrics(Collections.singletonList(metric));

        try {
            return service
                    .reports()
                    .batchGet(new GetReportsRequest().setReportRequests(Collections.singletonList(request)))
                    .execute().getReports().get(0).getData().getRows();
        } catch (IOException e) {
            log.error(e.getMessage());
            return null;
        }
    }

    /**
     * 광고 콘텐츠가 'urban-roi' 인 광고의 간접기여 조회
     * 각 List<Rows> 예시 :
     * [{"primitiveValue":"campaign-id"}, {"primitiveValue":"campaign-content"}, {"primitiveValue":"assistedValue"}]
     * @return
     */
    public List<List<McfData.Rows>> getIndirectList() {
        if (service == null) {
            log.error("service error");
            return null;
        }

        // filter =@ : substring
        // == : equals
        try {
            McfData response = mcfService.data().mcf()
                    .get("ga:" + viewId, "2021-12-20", "today", "mcf:assistedValue")
                    .setDimensions("mcf:adwordsCampaignID,mcf:campaignName")
                    .setFilters("mcf:campaignName==urban-roi")
                    .execute();
            return response.getRows();
        } catch (NullPointerException | IOException e) {
            return null;
        }
    }

    // test //
    public List<Report> test() {
        DateRange dateRange = new DateRange().setStartDate("2021-12-15").setEndDate("today");
        List<Dimension> dimensions = new ArrayList<>();
        List<DimensionFilter> filters = new ArrayList<>();

        dimensions.add(new Dimension().setName("ga:source"));
        dimensions.add(new Dimension().setName("ga:medium"));
        dimensions.add(new Dimension().setName("ga:adContent"));

        // PARTIAL =@ EXACT ==
        filters.add(new DimensionFilter().setDimensionName("ga:adContent").setOperator("EXACT").setExpressions(Collections.singletonList("urban-roi")));

        DimensionFilterClause dimensionFilter = new DimensionFilterClause().setFilters(filters);
        Metric metric = new Metric().setExpression("ga:users").setFormattingType("INTEGER");

        ReportRequest request = new ReportRequest()
                .setViewId(viewId)
                .setDateRanges(Collections.singletonList(dateRange))
                .setDimensions(dimensions)
                .setDimensionFilterClauses(Collections.singletonList(dimensionFilter))
                .setMetrics(Collections.singletonList(metric));

        try {
            return service
                    .reports()
                    .batchGet(new GetReportsRequest().setReportRequests(Collections.singletonList(request)))
                    .execute().getReports();
        } catch (IOException e) {
            log.error(e.getMessage());
            return null;
        }
    }


    /**
     * 소스/매체 보고서의 매체별 하루 총 매출 데이터 조회
     * @param source : pr_method - method (빈 문자열 "" 삽입시 medium 에 맞는 값 반환됨)
     *        medium : pr_method - medium (광고 : promotion, 홍보 : referral)
     * @return (String) 총 매출
     */
    public String getSingleDaySourceMediumData(String source, String medium) {
        if (service == null) return null;

        DateRange dateRange = new DateRange().setStartDate("1DaysAgo").setEndDate("today");
        List<Dimension> dimensions = new ArrayList<>();
        List<DimensionFilter> filters = new ArrayList<>();

        if (source.length() > 0) {
            dimensions.add(new Dimension().setName("ga:source"));
            filters.add(new DimensionFilter().setDimensionName("ga:source").setOperator("PARTIAL").setExpressions(Collections.singletonList(source)));
        }
        if (medium.length() > 0) {
            dimensions.add(new Dimension().setName("ga:medium"));
            filters.add(new DimensionFilter().setDimensionName("ga:medium").setOperator("EXACT").setExpressions(Collections.singletonList(medium)));
        }

        DimensionFilterClause dimensionFilter = new DimensionFilterClause().setFilters(filters).setOperator("AND");
        Metric metric = new Metric().setExpression("ga:transactionRevenue").setFormattingType("INTEGER");
        ReportRequest request = new ReportRequest()
                .setViewId(viewId)
                .setDateRanges(Collections.singletonList(dateRange))
                .setDimensions(dimensions)
                .setDimensionFilterClauses(Collections.singletonList(dimensionFilter))
                .setMetrics(Collections.singletonList(metric));

        try {
            return service
                    .reports()
                    .batchGet(new GetReportsRequest().setReportRequests(Collections.singletonList(request)))
                    .execute().getReports().get(0).getData().getTotals().get(0).getValues().get(0);
        } catch (IOException e) {
            log.error(e.getMessage());
            return null;
        }
    }

    /**
     * 소스/매체 보고서의 행 반환
     * @return List<ReportRow>
     */
    public List<ReportRow> getSingleDaySourceMediumRows() {
        if (service == null) {
            log.error("service error");
            return null;
        }

        DateRange dateRange = new DateRange().setStartDate("1DaysAgo").setEndDate("today");
        Dimension dimension = new Dimension().setName("ga:sourceMedium");
        Metric metric = new Metric().setExpression("ga:transactionRevenue").setFormattingType("INTEGER");

        ReportRequest request = new ReportRequest()
                .setViewId(viewId)
                .setDateRanges(Collections.singletonList(dateRange))
                .setDimensions(Collections.singletonList(dimension))
                .setMetrics(Collections.singletonList(metric));

        try {
            return service
                    .reports()
                    .batchGet(new GetReportsRequest().setReportRequests(Collections.singletonList(request)))
                    .execute().getReports().get(0).getData().getRows();
        } catch (IOException e) {
            log.error(e.getMessage());
            log.error("IOException");
            return null;
        }
    }

    /**
     * 다중 채널 유입경로 보고서 (MCF) 의 소스/매체 간접기여 조회
     * @return McfData
     *
     * McfData.getRows() : 소스/매체별 간접기여 조회
     *      [{"primitiveValue":"(direct) / (none)"}, {"primitiveValue":"2293900.0"}]
     *      [{"primitiveValue":"bing / organic"}, {"primitiveValue":"49300.0"}]
     *
     * McfData.getTotalsForAllResults() : 모든 간접기여 조회
     *      {mcf:assistedValue=8954200.0}
     */
    public McfData getSingleDayMcfData() {
        if (service == null) {
            log.error("service error");
            return null;
        }

        try {
            McfData response = mcfService.data().mcf().get(
              "ga:" + viewId, "1daysAgo", "today", "mcf:assistedValue"
            ).setDimensions("mcf:sourceMedium").execute();

            return response;
        } catch (IOException e) {
            log.error(e.getMessage());
            log.error("IOException");
            return null;
        }
    }

    /**
     * 다중 채널 유입경로 보고서의 간접기여 조회
     * @return
     */
    public Double getSingleDayMcfDataAssistedValue() {
        if (service == null) {
            log.error("service error");
            return null;
        }

        try {
            McfData response = mcfService.data().mcf().get(
                    "ga:" + viewId, "1daysAgo", "today", "mcf:assistedValue"
            ).setDimensions("mcf:basicChannelGroupingPath").execute();

            return Double.parseDouble(response.getTotalsForAllResults().get("mcf:assistedValue"));
        } catch (NullPointerException e) {
          return 0.0;
        } catch (IOException e) {
            log.error(e.getMessage());
            log.error("IOException");
            return null;
        }
    }
}
