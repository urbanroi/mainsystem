package com.urban.ROI_System.google_api.management;

import com.google.analytics.admin.v1alpha.*;
import com.google.api.gax.core.FixedCredentialsProvider;
import com.google.auth.oauth2.OAuth2Credentials;
import com.google.protobuf.FieldMask;
import com.urban.ROI_System.google_api.management.interfaces.AnalyticsAdmin;
import com.urban.ROI_System.util.ServerValue;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Slf4j
public class Accounts implements AnalyticsAdmin {

    private AnalyticsAdminServiceClient adminServiceClient;


    public Accounts() throws IOException {
        this.adminServiceClient = AnalyticsAdminServiceClient.create();
    } // Accounts
    public void terminate() throws InterruptedException {
        if (adminServiceClient != null) {
            adminServiceClient.shutdown();
            adminServiceClient.close();

            try {
                adminServiceClient.awaitTermination(5000, TimeUnit.MILLISECONDS);
            } catch (InterruptedException ex) {
                log.error("gRPC channel shutdown interrupted");
            }
        }
    }
    //에러나서 고쳐봄
    /*public void terminate() throws InterruptedException {
        if (adminServiceClient != null) {
            adminServiceClient.shutdown();
            adminServiceClient.awaitTermination(30, TimeUnit.SECONDS);
            adminServiceClient.close();
        }
    }*/

    /**
     * 현재 라이브러리로 사용할 수 없는 기능입니다
     */
    @Override
    public Boolean create() {
        return null;
    }

    @Override
    public List<?> getList() {
        ListAccountsRequest request = ListAccountsRequest.newBuilder().build();
        List<Account> response = adminServiceClient.listAccountsCallable().call(request).getAccountsList();

        return response;
    } // getList

    /**
     * @param accountName
     * @return Account
     */
    @Override
    public Object get(String accountName) {
        try { return adminServiceClient.getAccount("accounts/" + accountName); }
        catch (Exception e) {
            log.error(e.toString());
            return null;
        }
    } // get

    /**
     * @param params :
     *               String displayName : 계정명
     *               String countryCode : 소재국가 코드
     *               String name : 계정 고유번호
     * @return Boolean
     */
    @Override
    public Boolean update(Map<String, ?> params) {
        ArrayList<String> masks = new ArrayList<>();
        params.forEach((key, value) -> { if(params.get(key) != null) masks.add(key); });

        FieldMask updateMask = FieldMask.newBuilder().addAllPaths(masks).build();
        Account changeAccount = Account.newBuilder()
                .setName((String)params.get("name"))
                .setDisplayName((String)params.get("displayName"))
                .setCountryCode((String)params.get("countryCode"))
                .build();

        return adminServiceClient.updateAccount(changeAccount, updateMask) != null;
    } // update

    /**
     * @param accountName
     * @return Boolean
     */
    @Override
    public Boolean delete(String accountName) {
        if (get(accountName) == null) return false;

        AccountName name = AccountName.of(accountName);
        adminServiceClient.deleteAccount(name);

        return true;
    } // delete

    /**
     * AccountTicket 생성
     * @param credentials : OAuth2.0 토큰
     * @param displayName : 사용자 입력 계정이름
     * @return String accountToken
     */
    public String getAccountTicket(OAuth2Credentials credentials, String displayName) throws IOException {
        AnalyticsAdminServiceSettings settings = AnalyticsAdminServiceSettings.newBuilder().setCredentialsProvider(FixedCredentialsProvider.create(credentials)).build();
        AnalyticsAdminServiceClient client = AnalyticsAdminServiceClient.create(settings);
        Account account = Account.newBuilder().setDisplayName(displayName).setCountryCode("KR").build();

        String onlineURL = ServerValue.get().getOnlineURL();

        ProvisionAccountTicketRequest request = ProvisionAccountTicketRequest
                .newBuilder()
                .setAccount(account)
                .setRedirectUri(onlineURL+"/user/oauth/ga-create-success")
                .build();

        ProvisionAccountTicketResponse response = client.provisionAccountTicket(request);

        return response.getAccountTicketId();
    }

    /**
     * 액세스 토큰으로 계정 리스트 가져오기
     * @param credentials : OAuth2.0 토큰
     * @return List<Account>
     */
    public List<Account> getListByAccessToken(OAuth2Credentials credentials) throws IOException {
        AnalyticsAdminServiceSettings settings = AnalyticsAdminServiceSettings.newBuilder().setCredentialsProvider(FixedCredentialsProvider.create(credentials)).build();
        AnalyticsAdminServiceClient client = AnalyticsAdminServiceClient.create(settings);
        ListAccountsRequest request = ListAccountsRequest.newBuilder().build();

        try {
             return client.listAccountsCallable().call(request).getAccountsList();
        } catch (Exception e) {
            log.error(e.toString());
            return null;
        }
    }
}
