package com.urban.ROI_System.google_api.management;

import com.google.analytics.admin.v1alpha.AccountName;
import com.google.analytics.admin.v1alpha.AnalyticsAdminServiceClient;
import com.google.analytics.admin.v1alpha.ListUserLinksRequest;
import com.google.analytics.admin.v1alpha.UserLink;
import com.urban.ROI_System.google_api.management.interfaces.AnalyticsAdmin;
import com.urban.ROI_System.util.ServerValue;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Component
@NoArgsConstructor
@Slf4j
public class AccountsUserLinks implements AnalyticsAdmin {

    private AnalyticsAdminServiceClient adminServiceClient;
    private AccountName parent;
    private String serviceAccount;

    /**
     * @param accountName : Google Analytics Account ID
     * @throws IOException
     */
    public AccountsUserLinks(String accountName) throws IOException {
        this.adminServiceClient = AnalyticsAdminServiceClient.create();
        this.parent = AccountName.of(accountName);
        this.serviceAccount = ServerValue.get().getServiceAccount();
    } // AccountsUserLinks
    
    public void terminate() throws InterruptedException {
        if (adminServiceClient != null) {
            adminServiceClient.shutdown();
            adminServiceClient.close();

            try {
                adminServiceClient.awaitTermination(5000, TimeUnit.MILLISECONDS);
            } catch (InterruptedException ex) {
                log.error("gRPC channel shutdown interrupted");
            }
        }
    }

    //에러나서고쳐봄
    /*public void terminate() throws InterruptedException {
        if (adminServiceClient != null) {
            adminServiceClient.shutdown();
            adminServiceClient.awaitTermination(30, TimeUnit.SECONDS);
            adminServiceClient.close();
        }
    }*/

    /**
     * 애널리틱스 계정 관리자로 서비스계정 추가
     * @return Boolean
     */
    @Override
    public Boolean create() {
        log.info(serviceAccount);

        String[] roles = {"predefinedRoles/read", "predefinedRoles/collaborate", "predefinedRoles/edit", "predefinedRoles/manage-users"};
        UserLink userLink = UserLink.newBuilder().setEmailAddress(serviceAccount).addAllDirectRoles(Arrays.asList(roles)).build();
        UserLink response = adminServiceClient.createUserLink(parent, userLink);

        return response == null;
    } // create


    @Override
    public List<?> getList() {
        ListUserLinksRequest request = ListUserLinksRequest.newBuilder().setParent(parent.toString()).build();
        List<UserLink> response = adminServiceClient.listUserLinksCallable().call(request).getUserLinksList();

        //response.forEach(i -> log.info(i.toString()));
        return response;
    } // getList

    /**
     * 이메일로 UserLink 조회
     * @param email
     * @return UserLink
     */
    @Override
    public Object get(String email) {
        String userLinkId = getUserLinkId(getUserLinkId(email));
        if (userLinkId == null) return null;

        return adminServiceClient.getUserLink(userLinkId);
    } // get

    /**
     * 이메일 또는 권한 수정
     * @param params : String email, String updateEmail, String[] roles
     * @return Boolean
     */
    @Override
    public Boolean update(Map<String, ?> params){
        UserLink user = (UserLink) get((String)params.get("email"));
        if (user == null) return false;

        String email = (params.get("updateEmail") != null) ? (String)params.get("updateEmail") : user.getEmailAddress();
        String[] roles = (params.get("roles") != null) ? (String[])params.get("roles") : user.getDirectRolesList().toArray(new String[0]);
        UserLink changeUser = UserLink.newBuilder().setName(user.getName()).setEmailAddress(email).addAllDirectRoles(Arrays.asList(roles)).build();

        return adminServiceClient.updateUserLink(changeUser) != null;
    } // update

    /**
     * 이메일로 사용자 삭제
     * @param email
     * @return Boolean
     */
    @Override
    public Boolean delete(String email) {
        String userLinkId = getUserLinkId(email);
        if (userLinkId == null) return false;

        adminServiceClient.deleteUserLink(userLinkId);
        return true;
    } // delete

    /**
     * UserLink 고유번호 조회
     * @param email
     * @return String 고유번호
     */
    private String getUserLinkId(String email) {
        for (UserLink user : (List<UserLink>) getList()) {
            if (user.getEmailAddress().equals(email)) return user.getName();
        } // for

        return null;
    } // getUserLinkId

} // class