package com.urban.ROI_System.google_api.management;

import com.google.analytics.admin.v1alpha.*;
import com.google.protobuf.FieldMask;
import com.urban.ROI_System.google_api.management.interfaces.AnalyticsAdmin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class GAProperties implements AnalyticsAdmin {

    private AnalyticsAdminServiceClient adminServiceClient;
    private AccountName parent;

    /**
     * @param accountName : Google Analytics Account ID
     * @throws IOException
     */
    public GAProperties(String accountName) throws IOException {
        this.adminServiceClient = AnalyticsAdminServiceClient.create();
        this.parent = AccountName.of(accountName);
    } // Properties

    /**
     * 애널리틱스 속성 생성
     * @param params :
     *               String displayName : 속성명
     *               String industryCategory : 산업 카테고리
     *               String timeZone : 표준 시간대
     *               String currencyCode : 통화 유형
     * @return Boolean
     */
    public Boolean create(Map<String, String> params) {
        Property property = Property.newBuilder()
                .setParent(String.valueOf(parent))
                .setDisplayName(params.get("displayName"))
                .setIndustryCategory(IndustryCategory.valueOf(params.get("industryCategory")))
                .setTimeZone(params.get("timeZone"))
                .setCurrencyCode(params.get("currencyCode"))
                .build();

        return adminServiceClient.createProperty(property) != null;
    } // create

    @Override
    public Boolean create() {
        return null;
    }

    /**
     * 계정 ID 로 속성 목록 조회
     * @return List<Property>
     */
    @Override
    public List<?> getList() {
        ListPropertiesRequest request = ListPropertiesRequest.newBuilder().setFilter("parent:" + parent).build();
        return adminServiceClient.listPropertiesCallable().call(request).getPropertiesList();
    } // getList

    /**
     * 속성 ID 로 속성 조회
     * @param propertyName
     * @return Property
     */
    @Override
    public Object get(String propertyName) {
        PropertyName name = PropertyName.of(propertyName);
        return adminServiceClient.getProperty(name);
    }

    /**
     * 속성명 | 산업 예시 | 표준 시간대 | 통화 유형 수정
     * @param params :
     *               String name : 속성 고유번호
     *               String displayName : 속성명
     *               String industryCategory : 산업 카테고리
     *               String timeZone : 표쥰 시간대
     *               String currencyCode : 통화 유형
     * @return Boolean
     */
    @Override
    public Boolean update(Map<String, ?> params) {
        ArrayList<String> masks = new ArrayList<>();
        params.forEach((key, value) -> { if(params.get(key) != null) masks.add(key); });

        FieldMask updateMask = FieldMask.newBuilder().addAllPaths(masks).build();
        Property changeProperty = Property.newBuilder()
                .setName((String)params.get("name"))
                .setDisplayName((String)params.get("displayName"))
                .setCurrencyCode((String)params.get("currencyCode"))
                .setIndustryCategory(IndustryCategory.valueOf((String)params.get("industryCode")))
                .setTimeZone((String)params.get("timeZone"))
                .build();

        return adminServiceClient.updateProperty(changeProperty, updateMask) != null;
    }

    /**
     * PropertyName 으로 속성 삭제
     * @param propertyName
     * @return Boolean
     */
    @Override
    public Boolean delete(String propertyName) {
        if (get(propertyName) == null) return false;

        PropertyName name = PropertyName.of(propertyName);
        adminServiceClient.deleteProperty(name);
        return true;
    }
}
