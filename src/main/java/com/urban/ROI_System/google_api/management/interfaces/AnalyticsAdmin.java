package com.urban.ROI_System.google_api.management.interfaces;

import java.util.List;
import java.util.Map;

public interface AnalyticsAdmin {
    public Boolean create();
    public List<?> getList();
    public Object get(String param);
    public Boolean update(Map<String, ?> params);
    public Boolean delete(String param);
}
