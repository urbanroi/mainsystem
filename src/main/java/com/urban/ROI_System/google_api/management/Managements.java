package com.urban.ROI_System.google_api.management;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.analytics.Analytics;
import com.google.api.services.analytics.model.*;
import com.urban.ROI_System.util.ServerValue;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Slf4j
public class Managements {

    private static final JsonFactory JSON_FACTORY = GsonFactory.getDefaultInstance();
    private static String SERVICE_ACCOUNT = "";
    private Analytics service;

    /**
     * 계정, 속성, 보기 관리 클래스
     * @param tokenValue : 액세스 토큰
     */
    public Managements(String tokenValue) throws GeneralSecurityException, IOException {
        HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
        GoogleCredential credential = new GoogleCredential();
        credential.setAccessToken(tokenValue);
        credential.setExpiresInSeconds(3000L);

        this.service = new Analytics.Builder(httpTransport, JSON_FACTORY, credential).build();
        if (SERVICE_ACCOUNT.length() == 0) SERVICE_ACCOUNT = ServerValue.get().getServiceAccount();
    }

    /**
     * 계정 목록 조회
     * @return List<Account>
     */
    public List<Account> getAccounts() {
        List<Account> response;

        try { response = service.management().accounts().list().execute().getItems(); }
        catch (IOException e) {
            log.error(e.getMessage());
            return null;
        }
        return response;
    }

    /**
     * @param accountId : 계정 고유 번호
     * @return EntityUserLink
     */
    public EntityUserLink createAccountUserLink(String accountId) {
        UserRef userRef = new UserRef();
        userRef.setEmail(SERVICE_ACCOUNT);

        EntityUserLink.Permissions permissions = new EntityUserLink.Permissions();
        List<String> local = Arrays.asList("EDIT", "MANAGE_USERS");
        permissions.setLocal(local);

        EntityUserLink body = new EntityUserLink();
        body.setPermissions(permissions);
        body.setUserRef(userRef);

        EntityUserLink response;

        try { response = service.management().accountUserLinks().insert(accountId, body).execute(); }
        catch(Exception e) {
            log.error(e.getMessage());
            return null;
        }

        return response;
    }

    /**
     * 속성 및 보기 생성
     * @param accountId : 계정 고유 번호
     * @param webSiteUrl : 자사 쇼핑몰 URL
     * @param displayName : 속성 이름
     * @return List<Object> :
     *         List.get(0) : WebProperty
     *         List.get(1) : Profile
     * @throws IOException
     */
    public List<Object> create(String accountId, String webSiteUrl, String displayName) throws IOException {
        List<Object> result = new ArrayList<>();

        Webproperty property = new Webproperty();
        property.setWebsiteUrl(webSiteUrl);
        property.setName(displayName);

        Webproperty response;

        try {
            response = service.management().webproperties().insert(accountId, property).execute();
            result.add(response);
        }
        catch (IOException e) {
            log.error(e.getMessage());
            return null;
        }

        Profile profile = new Profile();
        profile.setName("전체 웹사이트 데이터");
        profile.setWebsiteUrl(webSiteUrl);
        profile.setTimezone("Asia/Seoul");
        profile.setCurrency("KRW");
        profile.setECommerceTracking(true);
        profile.setEnhancedECommerceTracking(true);

        result.add(service.management().profiles().insert(accountId, response.getId(), profile).execute());
        return result;
    }


    /**
     * 특정 속성 조회
     * @param accountId : 계정 고유 번호
     * @param propertyId : 속성 고유 번호
     * @throws IOException
     */
    public Webproperty get(String accountId, String propertyId) throws IOException {
        return service.management().webproperties().get(accountId, propertyId).execute();
    }

    public List<Webproperty> getProperties(String accountId) throws IOException {
        return service.management().webproperties().list(accountId).execute().getItems();
    }

}
