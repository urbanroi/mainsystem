package com.urban.ROI_System.google_api.google_tag_manager;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.tagmanager.TagManager;
import com.google.api.services.tagmanager.TagManagerScopes;
import com.google.api.services.tagmanager.model.*;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Slf4j
public class GTM {

    private static final String APPLICATION_NAME = "ROI System";
    public static String KEY_FILE_LOCATION;
    private TagManager service;
    private String path;
    private String folderId;

    public GTM() throws GeneralSecurityException, IOException {
        service = initialize();
    }

    static {
        String localPath = "src/main/java/com/urban/ROI_System/config/key/urbanroi-663efb743341.json";
        String serverPath = "apiKey.json";

        File file = new File(localPath);
        KEY_FILE_LOCATION =  file.exists() ? localPath : serverPath;
    }

    private static HttpRequestInitializer setHttpTimeout(final HttpRequestInitializer requestInitializer) {
        return new HttpRequestInitializer() {
            @Override
            public void initialize(HttpRequest request) throws IOException {
                requestInitializer.initialize(request);
                request.setConnectTimeout(5 * 60000);
                request.setReadTimeout(5 * 60000);
            }
        };
    }

    public TagManager initialize() throws IOException, GeneralSecurityException {
        HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();

        GoogleCredential credential = GoogleCredential
                .fromStream(new FileInputStream(KEY_FILE_LOCATION))
                .createScoped(TagManagerScopes.all());

        return new TagManager
                .Builder(httpTransport, GsonFactory.getDefaultInstance(), setHttpTimeout(credential))
                .setApplicationName(APPLICATION_NAME)
                .build();

        //return new TagManager(httpTransport, GsonFactory.getDefaultInstance(), setHttpTimeout(credential));
    }

    public void setParentPath(String accountId, String containerId) {
        this.path = "accounts/" + accountId + "/containers/" + containerId + "/workspaces/2";
    }
    public String getParentPath() {
        return path;
    }

    public void setFolderId(String folderId) {
        this.folderId = folderId;
    }

    public List<Account> getAccounts() throws IOException {
        return service.accounts().list().execute().getAccount();
    }

    public Account getAccount(String accountId) throws IOException {
        return service.accounts().get("accounts/" + accountId).execute();
    }

    public List<Container> getContainers(String accountId) throws IOException {
        return service.accounts().containers().list("accounts/" + accountId).execute().getContainer();
    }

    public Container getContainer(String accountId, String containerId) throws IOException {
        return service.accounts().containers().get("accounts/" + accountId + "/containers/" + containerId).execute();
    }

    public Container createContainer(String accountId, String name) throws IOException {
        Container container = new Container();
        container.setName(name);
        container.setUsageContext(Collections.singletonList("0"));

        return service.accounts().containers().create("accounts/" + accountId, container).execute();
    }

    public void deleteContainer(String accountId, String containerId) throws IOException {
        service.accounts().containers().delete("accounts/" + accountId + "/containers/" + containerId).execute();
    }

    public Folder createFolder(String folderName) throws IOException {
        if (path == null) return null;

        Folder folder = new Folder();
        folder.setName(folderName);

        return service.accounts().containers().workspaces().folders().create(path, folder).execute();
    }

    // GA Setting Variable :
    //      name : GA TRACKING ID
    //      type : gas
    //      key : trackingId
    //      value : UA Property ID

    // script Variable :
    //      name : variable name
    //      type : jsm
    //      key : javascript
    //      value : script
    public Variable createVariable(String name, String type, String key, String value) throws IOException {
        if (path == null) return null;

        Variable variable = new Variable();
        variable.setParentFolderId(folderId);
        variable.setName(name);
        variable.setType(type);
        variable.setParameter(Collections.singletonList(new Parameter().setType("template").setKey(key).setValue(value)));

        return service.accounts().containers().workspaces().variables().create(path, variable).execute();
    }

    public List<Trigger> getTriggers() throws IOException {
        if (path == null) return null;

        return service.accounts().containers().workspaces().triggers().list(path).execute().getTrigger();
    }

    public Trigger getTrigger(String triggerId) throws IOException {
        if (path == null) return null;

        return service.accounts().containers().workspaces().triggers().get(path + "/triggers/" + triggerId).execute();
    }

    public Trigger searchTriggerByName(String triggerName) throws IOException {
        if (path == null) return null;

        try {
            for (Trigger trigger : getTriggers())
                if (trigger.getName().equals(triggerName)) return trigger;
        } catch(NullPointerException ignored) { }
        return null;
    }

    // custom event trigger :
    //      name : trigger name
    //      type : customEvent
    //      filterType : equals
    //      filterParamKey : {{_event}}
    //      filterParamValue : event value (ex. viewCart, purchase ...)
    //      params : null

    // dom ready trigger :
    //      name : trigger name
    //      type : html
    //      filterType : equals
    //      filterParamKey : {{Page Path}}
    //      filterParamValue : path uri (ex. /product.detail.html ...)
    //      params : Collection.singletonList(new Parameter().setType("boolean").setKey("dom").setValue("false"))
    public Trigger createTrigger(String name, String type, String filterType, String filterParamKey, String filterParamValue, List<Parameter> params) throws IOException {
        if (path == null) return null;

        List<Parameter> filterParams = new ArrayList<>();
        filterParams.add(new Parameter().setType("template").setKey("arg0").setValue(filterParamKey));
        filterParams.add(new Parameter().setType("template").setKey("arg1").setValue(filterParamValue));

        List<Condition> filters = new ArrayList<>();
        filters.add(new Condition().setType(filterType).setParameter(filterParams));

        Trigger trigger = new Trigger();
        trigger.setParentFolderId(folderId);
        trigger.setName(name);
        trigger.setType(type);
        trigger.setParameter(params);

        if (type.equals("customEvent")) trigger.setCustomEventFilter(filters);
        if (filterParamKey != null) trigger.setFilter(filters);

        return service.accounts().containers().workspaces().triggers().create(path, trigger).execute();
    }

    // ua tag - track event
    //      name : tag name
    //      type : ua
    //      triggerId : trigger id
    //      params :
    //      String[][] uaTagParam = {
    //            {"template", "trackType", "TRACK_EVENT"},
    //            {"template", "trackingId", uaPropertyId},
    //            {"template", "eventCategory", "전자상거래"},
    //            {"template", "eventAction", null},
    //            {"template", "gaSettings", "{{[" + folderName + "] GA TRACKING ID}}"},
    //            {"boolean", "useEcommerceDataLayer", "true"},
    //            {"boolean", "enableEcommerce", "true"}
    //        };
    //      List<Parameter> params = new ArrayList<>();
    //      for (String[] p : uaTagParam) params.add(new Parameter().setType(p[0]).setKey(p[1]).setValue(p[2]));

    // ua tag - track page view
    //      name : tag name
    //      type : ua
    //      triggerId : 2147479553 (=> All Pages Trigger ID)
    //      params :
    //      uaTagParam 에서 trackType 을 "TRACK_PAGEVIEW" 로 변경
    //      ( 나머지는 track event 태그 params 와 동일 )

    // html tag - dom ready
    //      name : tag name
    //      type : html
    //      triggerId : trigger id
    //      params :  Collections.singletonList(new Parameter().setType("template").setKey("html").setValue(html source))
    public Tag createTag(String name, String type, String triggerId, List<Parameter> params) throws IOException {
        if (path == null) return null;

        Tag tag = new Tag();
        tag.setParentFolderId(folderId);
        tag.setName(name);
        tag.setType(type);
        tag.setParameter(params);
        tag.setFiringTriggerId(Collections.singletonList(triggerId));

        return service.accounts().containers().workspaces().tags().create(path, tag).execute();
    }

    // create container version
    public ContainerVersion createVersion() throws IOException {
        CreateContainerVersionRequestVersionOptions options = new CreateContainerVersionRequestVersionOptions();
        options.setName("version 1");

        return service
            .accounts()
            .containers()
            .workspaces()
            .createVersion(this.path, options)
            .execute()
            .getContainerVersion();
    }

    // publish container version
    public ContainerVersion versionPublish() throws IOException {
        if (path == null) return null;

        ContainerVersion containerVersion = createVersion();
        if (containerVersion == null) return null;

        log.info("version id: " + containerVersion.getContainerVersionId());

        return service
            .accounts()
            .containers()
            .versions()
            .publish(path.split("/workspace")[0] + "/versions/" + containerVersion.getContainerVersionId())
            .execute()
            .getContainerVersion();
    }

    public void deleteAllContainer(String accountId) throws IOException {
        List<Container> containers = service.accounts().containers().list("accounts/" + accountId).execute().getContainer();

        try {
            for (Container c : containers)
                service.accounts().containers().delete("accounts/" + accountId + "/containers/" + c.getContainerId()).execute();
        } catch (NullPointerException e) { }
    }
}
