package com.urban.ROI_System.util;

import com.urban.ROI_System.vo.ServerValueVO;
import lombok.experimental.UtilityClass;
import org.springframework.core.annotation.Order;

/**
 * 서버 변수를 가져오기 편하게 만든 객체
 */
@UtilityClass
public class ServerValue {
    /**
     * 서버 변수 VO 반환
     * @return 서버 변수 VO
     */
    @org.jetbrains.annotations.NotNull
    public static ServerValueVO get() {
        return BeanUtil.getBean(ServerValueVO.class);
    } //ServerValueVO();
} //ServerValue
