package com.urban.ROI_System.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.experimental.UtilityClass;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

@UtilityClass
public class BeanUtil {

    private static final ObjectMapper om = new ObjectMapper();
    //private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("roi");

    public static <T> T getBean(final Class<T> type) {
        return ApplicationContextProvider.getApplicationContext().getBean(type);
    }

    /*
    @Bean
    public static EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    @Bean
    public static EntityManagerFactory getEntityManagerFactory() {
        return emf;
    }
     */

    public static ObjectMapper getObjectMapper() { return om; }
} //BeanUtil