package com.urban.ROI_System.util;

import com.urban.ROI_System.model.ProjectModel;
import com.urban.ROI_System.vo.PatAndMat;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@NoArgsConstructor
@Slf4j
public class UTMBuilder {

    private StringBuilder sb = new StringBuilder();

    public UTMBuilder add(String key, String value) {
        if (sb.indexOf("?") != -1) {
            sb.append("&" + key + "=" + value);
        } else {
            sb.append("?" + key + "=" + value);
        }
        return this;
    } //add();

    public String makeUTM(ProjectModel projectModel, String source, String method) {
        log.info("makeUTM");

        String medium = method;
        String campaign = projectModel.getProjectName();

        /**
         * 소스/매체 직접입력이 아닌 경우
         * 캠페인명과 매체의 값을 바꾸어 utm 을 생성한다.
         */
        if (method.equals("P") || method.equals("A")) {
            medium = campaign;
            campaign = method.equals("P") ? "promotion" : "ads";
        }

        sb.append(projectModel.getLink());
        add("utm_campaign", campaign);
        sb.append("&utm_medium="+ medium);
        sb.append("&utm_source="+ source);
        sb.append("&utm_content="+ "urban-roi");
        sb.append("&utm_id="+ projectModel.getNo());

        return sb.toString();
    }

    public String updateUTM(ProjectModel projectModel) {
        log.info("updateUTM");
        String beforeUTM = projectModel.getUtm().split("&utm_medium")[1];

        sb.append(projectModel.getLink());
        add("utm_campaign", projectModel.getProjectName());
        sb.append("&utm_medium");
        sb.append(beforeUTM);

        return sb.toString();
    }

    /**
     * UTM 에서 패트와매트 추출
     * @param utm
     * @return 패트와매트
     */
    public PatAndMat getSourceMedium(String utm) {
        return new PatAndMat(utm.split("utm_source=")[0], utm.split("utm_medium=")[0]);
    }

    public boolean isPromotion(String utm) throws Exception {
        if(utm.indexOf("promotion") != -1) return true;
        return false;

        /*
        else {
            throw new Exception("광고홍보타입 불명 UTM");
        }
         */
    }

    /**
     * 프로젝트 리스트에서 소스, 매체가 일치하는 프로젝트 번호 추출
     * @param projects : 프로젝트 리스트
     * @param pat : 소스/매체 VO
     * @return Long 프로젝트 고유 번호
     */
    public Long getProjectByPatAndMat(List<ProjectModel> projects, PatAndMat pat) {
        for (ProjectModel project : projects) {
            if (project.getUtm().contains(pat.getMedium()) && project.getUtm().contains(pat.getMethod())) {
                return project.getNo();
            }
        }
        return null;
    }

    /**
     * 개발중
     *
     * @param key
     * @throws Exception
     */
    public void remove(String key) throws Exception {
        String param = sb.toString().split("/?")[0];
        param.split("&");

//        int keyIdx = sb.indexOf(key);
//        if (sb.indexOf("&", keyIdx) != -1) {
//            sb.delete(keyIdx-1, sb.length());
//        } else {
//            String z = sb.substring(keyIdx - 1);
//            if (z.equals("?")) {
//                z.indexOf("&");
//            } else if (z.equals("&")) {
//
//            } else {
//                throw new Exception("URL을 확인해주세요");
//            }
//        }
//        sb.indexOf("&", 2);
    } //remove();
}