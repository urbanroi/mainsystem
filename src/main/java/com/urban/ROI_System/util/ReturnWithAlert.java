package com.urban.ROI_System.util;

import org.springframework.web.servlet.ModelAndView;

public class ReturnWithAlert extends ModelAndView {
	
	public ReturnWithAlert(String message, String redirect) {
		addObject("message", message);
		addObject("redirect", redirect);
		addObject("title", "ROI-안내");
		
		setViewName("util/return_with_alert");
	} //RedirectWithAlert();
	
} //class RedirectWithAlert;