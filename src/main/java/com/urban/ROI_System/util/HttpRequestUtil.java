package com.urban.ROI_System.util;

import com.urban.ROI_System.vo.HttpResponseVO;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

public class HttpRequestUtil {

    private final String USER_AGENT = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:35.0) Gecko/20100101 Firefox/35.0";
    private final HttpURLConnection con;

    public HttpRequestUtil(String targetUrl) throws IOException {
        this.con = (HttpURLConnection) new URL(targetUrl).openConnection();
    } //ExecuteCrawling();

    public HttpResponseVO sendGet() throws IOException {
        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setDoOutput(false);
        con.setUseCaches(false);
        con.setDefaultUseCaches(false);
        con.setConnectTimeout(2000);
        con.disconnect();
        return new HttpResponseVO(getResponse(), con.getResponseCode());
    } //sendGet();

    public HttpResponseVO sendPost(String urlParam) throws IOException {
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setDoOutput(true); // POST 파라미터 전달을 위한 설정
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParam);
        wr.flush();
        wr.close();

        return new HttpResponseVO(getResponse(),con.getResponseCode());
    } // sendPost();

    public HttpResponseVO sendPostWithHeader(String urlParam, Map<String,String> map) throws IOException {
        for(String key : map.keySet()){
            System.out.println(key +" : "+ map.get(key));
            con.setRequestProperty(key, map.get(key));
        }
        return sendPost(urlParam);
    } // sendPost();

    private String getResponse() throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        StringBuffer response = new StringBuffer();
        String inputLine;
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        return response.toString();
    } // getResponse();
} //class