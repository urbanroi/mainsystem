package com.urban.ROI_System.util;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class Pagination {

    private final int btnSize = 10; // 페이징 버튼 개수
    private int size = 5;           // 페이지당 게시물수
    private int page;               // 현재 페이지
    private int startPage;          // 시작 페이지
    private int endPage;            // 끝 페이지
    private boolean prev;           // 이전
    private boolean next;           // 다음


    public Pagination(int page, Long total) {
        setPagination(page, total);
    }

    public Pagination(int page, Long total, int size) {
        this.size = size;
        setPagination(page, total);
    }

    private void setPagination(int page, Long total) {
        this.page = page;
        this.endPage = (int) Math.ceil(page / 10.0) * 10;
        this.startPage = this.endPage - (btnSize - 1);
        this.prev = this.startPage > 1;
        int realEnd = (int)Math.ceil((total * 1.0) / size);
        if (realEnd == 0) realEnd = 1;
        this.next = this.endPage < realEnd;
        if (realEnd < this.endPage) this.endPage = realEnd;
    }

}
