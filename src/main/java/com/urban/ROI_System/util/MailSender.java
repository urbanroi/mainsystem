package com.urban.ROI_System.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Component;

import javax.mail.internet.MimeMessage;

@Component("mailService")
@Slf4j
public class MailSender {

	@Autowired JavaMailSenderImpl mailSender;
	@Value("${onlineURL}")
	private String homeURL;

	public void sendMail(String email, int mailCode) {
		MimeMessagePreparator preparator = new MimeMessagePreparator() {

			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {
				String content =
					"<table style=\"width: 100%; font-size: 14px; color: #323232; text-align: center\">" +
							"      <tbody>" +
							"        <tr>" +
							"          <td>" +
							"            <table style=\"" +
							"                width: 700px;" +
							"                display: inline-table;" +
							"                text-align: left;" +
							"                padding-left: 50px;" +
							"                padding-right: 50px;" +
							"                padding-top: 50px;" +
							"                padding-bottom: 20px;\">" +
							"              <tbody>" +
							"                <tr>" +
							"                  <td style=\"text-align: center\" colspan=\"2\">" +
							"						<img src='https://dev.urbandigital.com/img/logo.png' width='100px'/>" +
							"                  </td>" +
							"                </tr>" +
							"              </tbody>" +
							"            </table>" +
							"          </td>" +
							"        </tr>" +
							"        <tr style=\"display: inline-grid\">" +
							"          <td style=\"height: 0\">" +
							"            <table" +
							"              style=\"" +
							"                width: 700px;" +
							"                display: inline-table;" +
							"                text-align: left;" +
							"                border-spacing: 0;" +
							"              \"" +
							"            >" +
							"              <tbody>" +
							"                <tr>" +
							"                  <td style=\"border-top: solid 4px black\" colspan=\"2\"></td>" +
							"                </tr>" +
							"              </tbody>" +
							"            </table>" +
							"          </td>" +
							"        </tr>" +
							"        <tr>" +
							"          <td>" +
							"            <table" +
							"              style=\"" +
							"                width: 700px;" +
							"                display: inline-table;" +
							"                text-align: left;" +
							"                padding-left: 50px;" +
							"                padding-right: 50px;" +
							"                padding-top: 20px;" +
							"                padding-bottom: 20px;" +
							"                box-shadow: 0px 3px 10px #ccc;" +
							"              \"" +
							"            >" +
							"              <tbody>" +
							"                <tr>" +
							"                  <td style=\"font-size: 16px\">Register email verification</td>" +
							"                  <td style=\"font-size: 14px; text-align: right\"></td>" +
							"                </tr>" +
							"                <tr>" +
							"                  <td" +
							"                    colspan=\"2\"" +
							"                    style=\"border-bottom: solid 1px #eee; padding-bottom: 20px\"" +
							"                  ></td>" +
							"                </tr>" +
							"                <tr>" +
							"                  <td colspan=\"2\" style=\"padding-top: 15px\">" +
							"                    현재 고객님의 메일로 인증번호가 신청되었습니다.인증번호를" +
							"                    입력하여 가입을 완료해 주시기 바랍니다" +
							"                  </td>" +
							"                </tr>" +
							"                <tr>" +
							"                  <td colspan=\"2\" style=\"padding-bottom: 30px\"></td>" +
							"                </tr>" +
							"                <tr>" +
							"                  <td" +
							"                    colspan=\"2\"" +
							"                    style=\"" +
							"                      border: solid 1px #ccc;" +
							"                      background: #eee;" +
							"                      font-size: 48px;" +
							"                      text-align: center;" +
							"                      font-weight: bold;" +
							"                      padding-top: 15px;" +
							"                      padding-bottom: 15px;" +
							"                    \"" +
							"                  >" +
							mailCode + "" +
							"                  </td>" +
							"                </tr>" +
							"                <tr>" +
							"                  <td colspan=\"2\" style=\"padding-bottom: 30px\"></td>" +
							"                </tr>" +
							"                <tr>" +
							"                  <td colspan=\"2\" style=\"padding-top: 15px\">" +
							"                    고객님 계정보안을 위해,인증번호는 3분간 유효하며 한번 사용된" +
							"                    번호는 효력을 잃게 됩니다." +
							"                  </td>" +
							"                </tr>" +
							"                <tr>" +
							"                  <td colspan=\"2\">" +
							"                    만약 본인이 신청한 것이 아니라면,어떠한 것도 진행" +
							"                    되지않으며, 안심하시고 이 메일을 무시해 주시기 바랍니다." +
							"                  </td>" +
							"                </tr>" +
							"                <tr>" +
							"                  <td colspan=\"2\" style=\"padding-bottom: 50px\"></td>" +
							"                </tr>" +
							"                <tr>" +
							"                  <td colspan=\"2\" style=\"color: #999\">" +
							"                    ROI 분석 시스템을 사용 해주셔서 감사합니다." +
							"                  </td>" +
							"                </tr>" +
							"                <tr>" +
							"                  <td colspan=\"2\" style=\"padding-bottom: 15px\"></td>" +
							"                </tr>" +
							"                <tr>" +
							"                  <td colspan=\"2\" style=\"color: #999\">" +
							"                    Access to the address：<a href=\""+ homeURL +"\"color:" +
							"                    #4791ff\" target=\"_blank\" rel=\"noreferrer noopener\"" +
							"                    >홈페이지" +
							"                  </td>" +
							"                </tr>" +
							"                <tr>" +
							"                  <td colspan=\"2\" style=\"padding-bottom: 15px\"></td>" +
							"                </tr>" +
							"                <tr>" +
							"                  <td colspan=\"2\" style=\"color: #999; font-size: 12px\">" +
							"                    Please do not reply to system mail" +
							"                  </td>" +
							"                </tr>" +
							"              </tbody>" +
							"            </table>";

				final MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
				helper.setFrom("UrbanRoiSystem");
				helper.setTo(email);
				helper.setSubject("ROI 이메일 인증 코드"); // 제목
				helper.setText(content, true); // 내용
			}
		};
		mailSender.send(preparator);
	}
}
